#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    utils.update_codes_by_dis_id
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Updates codes from csv file with code, current affiliate
    id, target affiliate id

    :copyright: (c) 2021 by BTS Software Solutions.
    :license: MIT License, see LICENSE for more details.
"""
import csv
import logging
import sys
from configparser import ConfigParser
from pathlib import Path

import coloredlogs
import plac
import psycopg2

logging.basicConfig(level=logging.DEBUG)
coloredlogs.install(level='DEBUG')


def db_config(filename):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)

    # get section, default to postgresql
    db = {}
    if parser.has_section('postgresql'):
        params = parser.items('postgresql')
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return db


def connect(db_conf):
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # connect to the PostgreSQL server
        logging.info('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**db_conf)

        # create a cursor
        cur = conn.cursor()

        # execute a statement
        cur.execute('SELECT version()')

        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        logging.info('PostgreSQL database version: %s' % db_version)

        return cur, conn
    except (Exception, psycopg2.DatabaseError) as error:
        logging.critical(error)
        if conn is not None:
            conn.close()
            logging.info('Database connection closed.')
        sys.exit("Could not connect to database.")


@plac.pos('input_file', "Input file containing code, cur_id, tar_id", type=Path)
@plac.pos('code_map_file', "File matching batch_ids to target affiliate ids", type=Path)
@plac.opt('database', "Which database to connect to {local, dev, prod}", type=str, choices=["local", "dev", "prod"], abbrev="db")
def main(input_file, code_map_file, database="local"):
    """Read codes and IDs from a CSV and updates them"""
    if not input_file.is_file():
        sys.exit("Input was not a file.")
    if not code_map_file.is_file():
        sys.exit("Code map was not a file.")
    if not input_file.suffix == '.csv':
        sys.exit("Input was not a CSV.")
    if not code_map_file.suffix == '.csv':
        sys.exit("Code map was not a CSV.")

    logging.info("Using %s db as target database." % database)
    if not Path("{}_db.ini".format(database)).is_file():
        sys.exit("Missing database config file: {}".format(database))

    db_conf = db_config('{}_db.ini'.format(database))
    cursor, conn = connect(db_conf)

    update_sql = \
        """
UPDATE app_ccf_vouchercode
SET batch_id = %s
WHERE code = %s;
        """

    try:
        code_map = {}
        input_map = {}
        updated_rows = 0
        with input_file.open() as inf, code_map_file.open() as cmf:
            code_reader = csv.DictReader(inf)
            map_reader = csv.DictReader(cmf)

            for line in map_reader:
                code_map.update({line['affiliate_id']: line['batch_id']})
            logging.debug(code_map)
            for line in code_reader:
                logging.info("Executing" + update_sql % (int(code_map[line['tar_id']]), line['code']))
                cursor.execute(update_sql, (code_map[line['tar_id']], line['code']))
                updated_rows += cursor.rowcount
            conn.commit()
            cursor.close()
            logging.info("Updated {} rows".format(updated_rows))
    except (Exception, psycopg2.DatabaseError) as error:
        logging.critical(error, exc_info=True)
        sys.exit("Exiting...")
    finally:
        if conn is not None:
            conn.close()
            logging.info("Database connection closed.")


if __name__ == "__main__":
    plac.call(main)
