# Utility Scripts
## Voucher Code
### `update_codes_by_dis_id.py`
#### Instructions
1. Create input_codes csv file with columns code,cur_id,tar_id
```csv
affiliate_id,batch_id
802122,13
802123,14
802124,15
```

2. Create batch-affiliate map csv file with columns affiliate_id,batch_id
```csv
code,cur_id,tar_id
dfxrfvxif,802124,802123
snkpoeagn,802122,802124
```

3. Create a prod, dev, or local db config file as in `local_db.ini`


```
usage: update_codes_by_dis_id.py [-h] [-db local] input_file code_map_file

Read codes and IDs from a CSV and updates them

positional arguments:
  input_file            Input file containing code, cur_id, tar_id
  code_map_file         File matching batch_ids to target affiliate ids

optional arguments:
  -h, --help            show this help message and exit
  -db local, --database local
                        Which database to connect to {local, dev, prod}
```
