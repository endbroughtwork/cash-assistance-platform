# -*- coding: utf-8 -*-
"""
    cash-assistance-platform.common
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Common classes on the site.

    :copyright: (c) 2021 BTS Software Solutions.
"""
from django import forms
from django.db import models
from django.utils.translation import ugettext_lazy as _


class TypeOfWork(models.TextChoices):
    """Applicant type of work."""
    HOUSE_CLEANING = 'House cleaning', _('house_cleaning')
    NANNY = 'Nanny', _('nanny')
    HOME_CARE = 'Home care', _('home_care')
    OTHER = 'Other', _("other_work")


YES_NO_CHOICES = ((None, ''), (True, _('yes')), (False, _('no')))


def clean_type_of_work_data(form_data):
    """Validate applicant type of work."""
    if not form_data:
        raise forms.ValidationError(_("field_required"))
    if form_data == TypeOfWork.OTHER:
        raise forms.ValidationError(_("work_eligible"))
    return form_data


def check_required(form_data):
    """Check if a form field is empty."""
    if form_data in (None, 'None', ''):
        raise forms.ValidationError(_("field_required"))
    return form_data
