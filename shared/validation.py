# -*- coding: utf-8 -*-
"""
    cash-assistance-platform.shared.validation
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Validation functions to unify front and backend
    validation.

    :copyright: (c) 2021 by Baltimore's Promise.
"""
import re

from django.core.validators import EmailValidator
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _


def street_address_validator():
    """
    Regex validation of a street address, matches full addresses or parts
    """
    return RegexValidator(r"^.[^?@{}();:“”@&*\"]*$", _('street_address_incorrect_format'))


def dirty_phone_number_validator():
    """
    Phone number validator, use only _AFTER_ cleaning a phone number of special characters,
    cleaned numbers contain no special characters, no country code
    """
    # Captures all valid US phone numbers from Stack Overflow (NANP):
    # https://stackoverflow.com/questions/123559/how-to-validate-phone-numbers-using-regex
    return RegexValidator(r"^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$", _('phone_number_incorrect_format'))


def clean_phone_number_validator():
    """
    Phone number validator, use only _AFTER_ cleaning a phone number of special characters,
    cleaned numbers contain no special characters, no country code, valid only for US numbers
    """
    return RegexValidator(r"^[1-9][0-9]{2}[0-9]{3}[0-9]{4}$", _('phone_number_incorrect_format'))


def text_characters_only_validator():
    """
    Regex validator that rejects special characters
    """
    #  no_special = re.compile(r"/^\w+$/mug")
    return RegexValidator(r"^[^\d]+$", _('no_special_characters_allowed'), flags=re.M | re.U)


def clean_zip_code_validator():
    """
    Regex validator that checks for US zipcodes in 5 or 9 digit format, no special or spaces
    """
    return RegexValidator(r"^(\d{9}|\d{5})?$", _('zip_code_incorrect_format'))


def dirty_zip_code_validator():
    """
    Regex validator that checks for US zipcodes in 5 or 9 digit format, allows dash and spaces between parts
    """
    return RegexValidator(r"^((\d{9}|\d{5}[-\s]*)\d{4}|\d{5})?$", _('zip_code_incorrect_format'))


def email_validator():
    """
    Regex validator for email addresses, whitelists common email domains for faster checking
    """
    return EmailValidator(_("invalid_email"),
                          whitelist=["gmail.com", "outlook.com", "live.com", "hotmail.com", "yahoo.com"])


def dirty_voucher_validator_v1():
    """Regex validator that checks for a valid code, expects a 9-26 code, excluding 'l' and 'L'"""
    return RegexValidator(r"^(-?[a-km-zA-KM-Z]{1,3}-?){3,7}$", _('voucher_code_invalid_format'))


def clean_voucher_validator_v1():
    """Regex validator that checks for a valid code, expects a 9-26 code, excluding 'l' and 'L'"""
    return RegexValidator(r"^(-?[a-km-zA-KM-Z]{1,3}-?){3,7}$", _('voucher_code_invalid_format'))


def clean_number_only_validator():
    return RegexValidator(r"\d{10}")
