# -*- coding: utf-8 -*-
"""
    cash-assistance-platform.shared.utils
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Utility functions used throughout the app.
"""
from contextlib import contextmanager

from django.utils import translation


def visitor_ip_address(request):
    """Extract visitor IP from browser data or stored application history data"""
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        remote_addr = x_forwarded_for.split(',')[0]
    else:
        remote_addr = request.META.get('REMOTE_ADDR')
    return remote_addr


def clean_phone_number(dirty_phone_number):
    """Returns a cleaned version of a user-provided phone number."""
    trs_tbl = {'-': '', '(': '', ')': '', ' ': '', '+': ''}
    return dirty_phone_number.translate(str.maketrans(trs_tbl))


def clean_zip_code(dirty_zip_code):
    """Returns a cleaned version of a user-provided zip code."""
    trs_tbl = {'-': '', '(': '', ')': '', ' ': '', '+': ''}
    return dirty_zip_code.translate(str.maketrans(trs_tbl))


def to_user_facing_code(code):
    """Returns a user-facing code given a raw code (e.g., abcdefghij)."""
    return '{}-{}-{}'.format(code[:3], code[3:6], code[6:])


def clean_voucher_code(dirty_code):
    """Cleans voucher codes of special characters

    :dirty_code: str dirty code, possibly with special characters
    :returns: str Cleaned code without special characters

    """
    trs_tbl = {'-': '', ' ': ''}
    return dirty_code.translate(str.maketrans(trs_tbl)).lower()


@contextmanager
def activate_language(language):
    """A context manager for temporarily activating a language in the env."""
    cur_language = translation.get_language()
    translation.activate(language)
    try:
        yield None
    finally:
        translation.activate(cur_language)
