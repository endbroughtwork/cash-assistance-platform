invoke
mock
parameterized
phonenumbers
psycopg2-binary
unidecode
boto3
aws-xray-sdk
ratelimit
backoff
requests


Django
django-admin-honeypot
django-filter
django-localflavor
django-session-security
django-two-factor-auth
django-widget-tweaks
django-storages
django-tables2

django-debug-toolbar
djangorestframework
