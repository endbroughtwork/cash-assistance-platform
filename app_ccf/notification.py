# -*- coding: utf-8 -*-
"""
    cash-assistance-platform.notification
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Send a text message notification of registration.

    :copyright: (c) 2021 by YOUR_NAME.
    :license: LICENSE_NAME, see LICENSE for more details.
"""
import json
from collections.abc import Iterable
from enum import Enum

import boto3

import ccf.settings as conf
from app_ccf.config import get_config


lambda_client = boto3.client('lambda', region_name='us-east-1')


class TextType(Enum):
    """Text message type selector"""
    SUBMISSION = 1
    SENT_FOR_PAYMENT = 2
    REJECTION = 3


def send_text(applications, text_type):
    """Send text for one or multiple applications.

    Args:
        applications: Application object or an iterable of applications
    """
    if not isinstance(applications, Iterable):
        applications = (applications,)

    if text_type == TextType.SUBMISSION:
        for application in applications:
            sms_info = {
                'appId': str(application.application_id),
                'destinationNumber': '+1' + str(application.phone_number),
                'firstName': str(application.first_name),
                'lastName': str(application.last_name),
                'language': str(application.language),
                'received': "false",
                'source': str(get_config()['fund_name']),
                'stage': conf.STAGE
            }
            json_str = json.dumps(sms_info)
            lambda_client.invoke(
                FunctionName='sms-{}-registration'.format(conf.STAGE),
                InvocationType='Event',
                LogType='Tail',
                Payload=json_str,
            )

    if text_type == TextType.SENT_FOR_PAYMENT:
        for application in applications:
            sms_info = {
                'destinationNumber': '+1' + str(application.phone_number),
                'language': str(application.language),
                'stage': conf.STAGE
            }
            json_str = json.dumps(sms_info)
            lambda_client.invoke(
                FunctionName='sms-{}-sent-payment'.format(conf.STAGE),
                InvocationType='Event',
                LogType='Tail',
                Payload=json_str,
            )

    if text_type == TextType.REJECTION:
        for application in applications:
            sms_info = {
                'destinationNumber': '+1' + str(application.phone_number),
                'language': str(application.language),
                'stage': conf.STAGE
            }
            json_str = json.dumps(sms_info)
            lambda_client.invoke(
                FunctionName='sms-{}-rejected-app'.format(conf.STAGE),
                InvocationType='Event',
                LogType='Tail',
                Payload=json_str,
            )
