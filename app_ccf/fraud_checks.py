from django.db.models import Q

from app_ccf.models import Application
from app_ccf.models import PreapprovedAddress


class BaseDedupCheck:
    """
    Base class for checking Applications for duplicates.

    Subclasses should override get_test_value to return a value
    to be compared across applications for duplicates. They may
    optionally provide max_duplicates if the number of allowed
    duplicates is greater than 1.

    Args:
        error_message (str): The note to append to the application when the
            dedup check fails.
        max_duplicates (int): The number of applications that are allowed to
            share a common test value before the dedup check fails.
        new_status (Application.Status): The status to update the application
            to if the dedup check fails.

    """

    def __init__(self, error_message, max_duplicates=1,
                 new_status=Application.ApplicationStatus.NEEDS_REVIEW):
        self.max_duplicates = max_duplicates
        self.error_message = error_message
        self.new_status = new_status

    def get_max_duplicates(self):
        """Returns the max number of apps that may share the value tested."""
        return self.max_duplicates

    def get_error_message(self):
        """Returns the error message to use when duplicates are detected."""
        return self.error_message

    def is_preapproved(self, application):
        """Returns whether this application is exempted from this check."""
        return False

    def get_test_value(self, application):
        """Returns the standardized value to be tested for duplicates."""
        pass


def is_not_maryland(candidate):
    """Checks whether an address is in Maryland

    :candidate: Application
    :returns: bool

    """
    qs = PreapprovedAddress.objects.filter(
        addr2__iexact=candidate.addr1, zip_code=candidate.zip_code)
    # NOTE: if an address is already preapproved, short circuit then return False
    if qs.exists():
        return None
    if candidate.state.lower() != 'md':
        return True
    return None


def is_duplicated_email(candidate):
    """check if contact info is duplicated either phone OR email

    :candidate: Application
    :returns: bool

    """
    qs = Application.objects.filter(email__iexact=candidate.email)
    if candidate.email != "" and candidate.email is not None and qs.exists():
        return qs
    return None


def is_duplicated_phone(candidate):
    """check if contact info is duplicated either phone OR email

    :candidate: Application
    :returns: bool

    """
    qs = Application.objects.filter(phone_number=candidate.phone_number)
    if qs.exists():
        return qs
    return None


def is_duplicated_name(candidate):
    """check if first_name, last_name tuple exist

    :candidate: Application
    :returns: bool

    """
    qs = Application.objects.filter(first_name__iexact=candidate.first_name,
                                    last_name__iexact=candidate.last_name)
    if qs.exists():
        return qs
    else:
        return None


def is_duplicated_addr(candidate):
    """check if an applicant address is duplicated

    :candidate: Application
    :returns: bool

    """
    qs = PreapprovedAddress.objects.filter(
        addr2__iexact=candidate.addr1, zip_code=candidate.zip_code)
    # NOTE: if an address is already preapproved, short circuit then return False
    if qs.exists():
        return None
    # check if addr1, addr2, zip, city, state 5-tuple exist
    qs = Application.objects.filter(addr1__iexact=candidate.addr1,
                                    addr2__iexact=candidate.addr2,
                                    city__iexact=candidate.city,
                                    zip_code=candidate.zip_code,
                                    state__iexact=candidate.state)
    if qs.exists():
        return qs
    return None


class AddressDedupCheck(BaseDedupCheck):
    """Flags duplicate addresses across more than 5 apps for review."""

    def __init__(self):
        super().__init__(error_message='duplicate address', max_duplicates=3)

    def is_preapproved(self, application):
        return PreapprovedAddress.objects.filter(
            addr2=application.addr1, zip_code=application.zip_code).exists()

    def get_test_value(self, application):
        return '\n'.join([application.addr1,
                          application.city,
                          application.state,
                          application.zip_code])


class NamePhoneDedupCheck(BaseDedupCheck):
    """Rejects apps with the same first name, last name, and phone number."""

    def __init__(self):
        super().__init__(error_message='duplicate first/last/phone',
                         new_status=Application.ApplicationStatus.NEEDS_REVIEW)

    def get_test_value(self, application):
        return application.first_name.lower() + application.last_name.lower() + \
            application.phone_number
