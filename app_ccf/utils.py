import logging
import urllib.request
import xml.etree.ElementTree as ET

from django.conf import settings
#  from backoff import expo
#  from backoff import on_exception
#  from ratelimit import limits
#  from ratelimit import RateLimitException
#  import unidecode
#  from django.core.validators import RegexValidator
#  from django.utils.translation import ugettext_lazy as _

LOGGER = logging.getLogger(__name__)


#  @on_exception(expo, RateLimitException, max_tries=2)
#  @limits(calls=2, period=2)
def verify_usps_addr(entered_addr_1, entered_addr_2, entered_city, entered_state, entered_zip):
    requestXML = """<?xml version="1.0" encoding="UTF-8" ?>
    <AddressValidateRequest USERID="%s">
        <Address>
            <Address1>%s</Address1>
            <Address2>%s</Address2>
            <City>%s</City>
            <State>%s</State>
            <Zip5>%s</Zip5>
            <Zip4/>
        </Address>
    </AddressValidateRequest>""" % (settings.USPS_USER_ID, entered_addr_2,
                                    entered_addr_1, entered_city, entered_state,
                                    entered_zip)
    request_args = {
        'API': 'Verify',
        'XML': requestXML
    }

    url = "https://secure.shippingapis.com/ShippingAPI.0dll?" + \
        urllib.parse.urlencode(request_args)
    response = urllib.request.urlopen(url)
    if response.getcode() != 200:
        LOGGER.error('#UspsHttpError: ' + response.info())
        raise Exception('USPS HTTP error')

    contents = response.read()
    root = ET.fromstring(contents)

    address = root.find('Address')
    # There should only be one <Address> tag, return the first one.
    error_description = ""
    # check if it contains an error message
    if address.find("Error"):
        error_description = address.find("Error").findtext("Description",
                                                           default="")
    if address:
        return (address.findtext("Address2", default=""),
                address.findtext("Address1", default=""),
                address.findtext("City", default=""),
                address.findtext("State", default=""),
                address.findtext("Zip5", default=""),
                address.findtext("ReturnText", default=""),
                error_description)
    # in case xml returned is invalid, log and raise exception
    else:
        LOGGER.error('#UspsHttpError: xml missing Address element ' + response.info())
        raise Exception('USPS HTTP error')
