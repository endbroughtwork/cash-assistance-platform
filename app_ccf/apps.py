"""
Application Configuration and Settings
"""
from django.apps import AppConfig


class AppCcfConfig(AppConfig):
    """
    Default
    """
    name = 'app_ccf'
