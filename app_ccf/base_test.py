
from django.test import TestCase
import mock

import time


class CcfBaseTest(TestCase):

    def setUp(self):
        super().setUp()
        if not hasattr(time.sleep, 'mock'):
            mock.patch.object(time, 'sleep', autospect=True).start()
