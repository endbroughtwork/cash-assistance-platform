from django.conf import settings
from django.utils import translation
from django.utils.translation import ugettext_lazy as _


def get_translation_in(language, s):
    with translation.override(language):
        return translation.gettext(s)


def get_config():
    return {
        'languages': ('en', 'es'),
        'site_classname': 'ccf-myalia-org',
        'mayors_site_link': _('mayors_site_link'),
        'mayors_site_name': _('mayors_site_name'),
        'tos_site_link': _('tos_site_link'),
        'tos_site_name': _('tos_site_name'),
        'privacy_site_link': _('privacy_site_link'),
        'privacy_site_name': _('privacy_site_name'),
        'fund_name': _('ccf_fund_name'),
        'fund_name_es': get_translation_in('es', 'ccf_fund_name'),
        'fund_name_en': get_translation_in('en', 'ccf_fund_name'),
        'faq_link_url': _('ccf_faq_link'),
        'faq_link_text': _('ccf_faq').format(fund_name=settings.FUND_NAME),
        'tos_link_url': _('signature_tos_link'),
        'tos_link_text': _('signature_terms_and_conditions_carefund').format(fund_name=settings.FUND_NAME),
        'example_city_zip_code': '21217',
        'city_name': 'Baltimore',
        'payment_amount': '400',
        'customer_service_phone_number': '+1 (443) 256-9289',
        'eligibility_requirement_list': [_('requirements_eighteen_or_older'),
                                         _('requirements_income_source'),
                                         _('requirements_experiencing_hardship'),
                                         _('requirements_live_in_us')],
        'usio_card_design_id_en': '425',
        'usio_card_design_id_es': '426',
    }
