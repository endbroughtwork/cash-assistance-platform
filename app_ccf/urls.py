from django.urls import path
from django.urls import re_path

from . import views

urlpatterns = [
    path('language', views.LanguageView.as_view(), name='language'),
    re_path(
        r'^language/(?P<accesscode>([a-zA-Z]{3}-[a-zA-Z]{3}-[a-zA-Z]{3}))/$',
        views.LanguageView.as_view(),
        name='language'
    ),
    path('welcome',
         views.WelcomeView.as_view(next_name='accesscode', previous_name='language', step_number=1),
         name='welcome'),
    path('accesscode',
         views.VoucherCodeView.as_view(next_name='mailing-address', previous_name='welcome', step_number=2),
         name='accesscode'),
    path('mailing-address',
         views.AddressView.as_view(next_name='terms-and-conditions', previous_name='accesscode', step_number=3),
         name='mailing-address'),
    path('usps-modified-address',
         views.AddressVerifyView.as_view(next_name='terms-and-conditions',
                                         previous_name='mailing-address',
                                         step_number=3),
         name='usps-modified-address'),
    path('usps-missing-apt', views.AddressMissingAptView.as_view(step_number=3),
         name='usps-missing-apt'),
    path('usps-cannot-verify', views.AddressUnverifiedView.as_view(step_number=3),
         name='usps-cannot-verify'),
    path('terms-and-conditions',
         views.TermsConditionsView.as_view(next_name='eligibility', previous_name='mailing-address', step_number=4),
         name='terms-and-conditions'),
    path('eligibility',
         views.EligibilityView.as_view(next_name='review', previous_name='terms-and-conditions', step_number=5),
         name='eligibility'),
    path('review',
         views.ReviewView.as_view(next_name='success', previous_name='eligibility', step_number=6),
         name='review'),
    path('success',
         views.ConfirmationView.as_view(next_name='language', previous_name='review', step_number=7),
         name='success'),
    path('no-code',
         views.NoVoucherCodeView.as_view(),
         name='no-code'),
]
