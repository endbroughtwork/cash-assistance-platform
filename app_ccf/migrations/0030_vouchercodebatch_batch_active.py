# Generated by Django 3.1.6 on 2021-02-11 19:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_ccf', '0029_auto_20210211_1558'),
    ]

    operations = [
        migrations.AddField(
            model_name='vouchercodebatch',
            name='batch_active',
            field=models.BooleanField(default=True, verbose_name='Batch Active'),
        ),
    ]
