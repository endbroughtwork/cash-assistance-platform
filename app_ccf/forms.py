"""
Apllicant form components
"""
from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import MaxLengthValidator
from django.forms.models import model_to_dict
from django.utils import translation
from django.utils.translation import ugettext_lazy as _
from localflavor.us.forms import USStateField
from localflavor.us.us_states import US_STATES

from .models import Application
from shared.common import clean_type_of_work_data
from shared.validation import dirty_phone_number_validator
from shared.validation import dirty_voucher_validator_v1
from shared.validation import dirty_zip_code_validator
from shared.validation import email_validator
from shared.validation import text_characters_only_validator


class ApplicationForm(forms.ModelForm):
    class Meta:
        model = Application
        fields = '__all__'


class RequirementsForm(forms.Form):
    qualified = forms.BooleanField(
        widget=forms.RadioSelect(
            choices=[
                (True, _('yes')), (False, _('no'))]))


class VoucherCheckForm(forms.Form):
    vouchercheck = forms.BooleanField(
        widget=forms.RadioSelect(
            choices=[
                (True, _('yes')), (False, _('no'))]))


class VoucherCodeForm(forms.Form):
    voucher_input = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control',
                   'maxlength': '26',  # max 20 chars + 6 dashes
                   'minlength': '9',  # min chars 9 and no dashes
                   'onkeypress': 'return isVoucherKey(event);',
                   'autocomplete': 'off'
                   }),
        validators=[dirty_voucher_validator_v1()],
    )


class ProfileForm(ApplicationForm):
    fund_name = ''

    def __init__(self, fund_name, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.fund_name = fund_name

    first_name = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'maxlength': '200'}),
        validators=[text_characters_only_validator()],
    )
    last_name = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'maxlength': '200'}),
        validators=[text_characters_only_validator()],
    )

    class Meta:
        model = Application
        fields = ['age_range']
        widgets = {
            'age_range': forms.RadioSelect(),
        }

    def clean_age_range(self):
        data = self.cleaned_data['age_range']
        if data == Application.AgeRange.RANGE__17:
            raise forms.ValidationError(_("older_18").format(fund_name=self.fund_name))
        return data


def get_states_in(language):
    """Returns US states in the provided language, forcing eager evaluation."""
    with translation.override(language):
        return list((abbr, str(name)) for abbr, name in US_STATES[:])


class AddressForm(ApplicationForm):

    # combined all applicant info into this single form per new requirements.
    class Meta:
        model = Application
        fields = ['addr1', 'addr2', 'city', 'state', 'zip_code', 'first_name',
                  'last_name', 'phone_digits', 'email']
        widgets = {
            'addr1': forms.TextInput(attrs={'class': 'form-control'}),
            'addr2': forms.TextInput(attrs={'class': 'form-control'}),
            'city': forms.TextInput(attrs={'class': 'form-control'}),
        }

    zip_code = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'maxlength': '10',  # 5 digit short zip + dash + 4 digit ext
                'minlength': '5',  # 5 digit short zip
            }
        ),
        validators=[dirty_zip_code_validator()],
    )
    # Hack to deal with issue that UsStateSelect does not contain
    # a default value (https://stackoverflow.com/questions/1830894).
    state_choices = get_states_in('en')
    state_choices.insert(0, ('', _('select_one')))
    state = USStateField(widget=forms.Select(
        attrs={'class': 'form-control'}, choices=state_choices))

    # added first name and last name, phone number and email address
    first_name = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'maxlength': '20'}),
        validators=[text_characters_only_validator()],
    )
    last_name = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'maxlength': '30'}),
        validators=[text_characters_only_validator()],
    )
    phone_digits = forms.CharField(
        required=True,
        max_length=20,  # county + area + 7 digits with spaces, plus, dashes
        min_length=10,  # area + 7 digits, no country code, no special
        widget=forms.TextInput(attrs={
            'type': 'tel',
            'class': 'form-control',
            'maxlength': '20',  # county + area + 7 digits with spaces, plus, dashes
            'minlength': '10',  # area + 7 digits, no country code, no special
            'onkeypress': 'return isNumberKey(event);'
        }),
        validators=[dirty_phone_number_validator()],
    )
    email = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'type': 'email',
            'class': 'form-control',
        }),
        validators=[email_validator()],
    )


class AddressVerifyForm(ApplicationForm):
    class Meta:
        model = Application
        fields = ['usps_standardized']


class TermsConditionsForm(forms.Form):
    tos = forms.BooleanField(required=True)


class EligibilityForm(ApplicationForm):
    class Meta:
        model = Application
        fields = ['signature']
        widgets = {
            'signature': forms.TextInput(attrs={'class': 'form-control'}),
        }


class ReviewForm(forms.Form):
    pass


class WelcomeForm(forms.Form):
    pass
