"use strict";
const AWS = require("aws-sdk");
const AWSXRay = require("aws-xray-sdk");
const axios = require("axios");
const convert = require("xml-js");
AWSXRay.captureHTTPsGlobal(require("http")); // Globally instrument http client
AWSXRay.captureHTTPsGlobal(require("https")); // Globally instrument https client
AWSXRay.capturePromise();

const http = require("http");
const https = require("https");

const instance = axios.create({
  httpAgent: new http.Agent(),
  httpsAgent: new https.Agent(),
});

const user = process.env.USIO_USER;
const pass = process.env.USIO_PASS;
const usioUrl =
  "https://gateway.cardbillpay.com/cardservices/transferSvc.asmx?op=createConsumer";

exports.handler = async (event) => {
  console.log("Received event:", event);
  const returnObj = { statusCode: 200 };

  try {
    const { body } = event;
    const reqBody = body.replace("uname", user).replace("pwd", pass);
    const resp = await instance.post(usioUrl, reqBody, {
      headers: {
        "Content-Type": "application/soap+xml; charset=utf-8;",
      },
    });
    const respJs = convert.xml2js(resp.data, { compact: true });
    console.log(
      respJs["soap:Envelope"]["soap:Body"].createConsumerResponse
        .createConsumerResult
    );
    returnObj.result =
      respJs["soap:Envelope"][
        "soap:Body"
      ].createConsumerResponse.createConsumerResult;
    return returnObj;
  } catch (err) {
    console.error(err);
    throw err;
  }
};
