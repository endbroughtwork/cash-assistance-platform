const AWS = require("aws-sdk");
const AWSXRay = require("aws-xray-sdk");
// const pg = AWSXRay.capturePostgres(require("pg"));
const pp = AWSXRay.captureAWSClient(
  new AWS.Pinpoint({ region: process.env.region })
);

const pinpointAppDev = process.env.projectIdDev;
const pinpointAppProd = process.env.projectIdProd;
const pinpointApp = {
  dev: pinpointAppDev,
  prod: pinpointAppProd,
};
const OriginationNumber = process.env.originationNumber;
const messageType = "PROMOTIONAL";

const msgTextEn =
  "Baltimore Responds has submitted your application for payment. " +
  "You should receive your card in the mail within 2-3 weeks. " +
  "We will check weekly via text message whether you have received " +
  "your card.";
const msgTextEs =
  "Baltimore Responds ha enviado su solicitud de pago. " +
  "Debería recibir su tarjeta por correo dentro de 2-3 semanas. " +
  "Vamos a comprobar semanalmente a través de mensaje de texto si ha " +
  "recibido su tarjeta.";
const msgText = {
  en: msgTextEn,
  es: msgTextEs,
};

exports.handler = async (event) => {
  console.log("Received event:", event);
  const returnObj = { statusCode: 200 };
  try {
    let userPhone = event.destinationNumber;
    const { language, stage } = event;
    if (userPhone.length == 10) userPhone = "+1" + userPhone;
    const validateParams = {
      NumberValidateRequest: { IsoCountryCode: "US", PhoneNumber: userPhone },
    };
    const validated = await pp.phoneNumberValidate(validateParams).promise();
    console.log(validated);
    if (
      validated["NumberValidateResponse"]["PhoneTypeCode"] == 0 ||
      validated["NumberValidateResponse"]["PhoneTypeCode"] == 2 ||
      validated["NumberValidateResponse"]["PhoneTypeCode"] == 5
    ) {
      // createEndpoint(validated, event.firstName, event.lastName, event.source);
      userPhone =
        validated["NumberValidateResponse"]["CleansedPhoneNumberE164"];
      const endpointId = validated["NumberValidateResponse"][
        "CleansedPhoneNumberE164"
      ].substring(1);
      const sendParams = {
        ApplicationId: pinpointApp[stage],
        MessageRequest: {
          Addresses: {
            [userPhone]: {
              ChannelType: "SMS",
            },
          },
          MessageConfiguration: {
            SMSMessage: {
              Body: `${msgText[language]}`,
              MessageType: messageType,
              OriginationNumber,
            },
          },
        },
      };
      console.log("Sending message:", sendParams);
      const response = await pp.sendMessages(sendParams).promise();
      console.log(response.MessageResponse);
      console.log(JSON.stringify(response.MessageResponse, null, 2));
      return returnObj;
    }
    returnObj.statusCode = 500;
    returnObj.message =
      "Could not send a text message: Failed phone number validation.";
    return returnObj;
  } catch (err) {
    console.error(err);
    returnObj.statusCode = 500;
    returnObj.message = `Could not send a text message: ${err.message}`;
  }
};
