const AWS = require("aws-sdk");
const AWSXRay = require("aws-xray-sdk");
const pg = AWSXRay.capturePostgres(require("pg"));
const pp = AWSXRay.captureAWSClient(
  new AWS.Pinpoint({ region: process.env.region })
);

const pinpointAppDev = process.env.projectIdDev;
const pinpointAppProd = process.env.projectIdProd;
const pinpointApp = {
  dev: pinpointAppDev,
  prod: pinpointAppProd,
};
const optInKeyword = process.env.optInKeyword.toLowerCase();
const optOutKeyword = process.env.optOutKeyword.toLowerCase();
const recvKeyword = process.env.recvKeyword.toLowerCase();
const notRecvKeyword = process.env.notRecvKeyword.toLowerCase();
const helpKeyword = process.env.helpKeyword.toLowerCase();
const helpTextEn =
  "Text YES when you have received your prepaid card. " +
  "Text DONE to opt-out of receiving messages. " +
  "Text NO if you have not received your prepaid card. " +
  "Text HELP to repeat this message.";
const helpTextEs =
  "Envíe SI cuando hayas recibido su tarjeta prepagada. " +
  "Envíe DONE para optar por no recibir mensajes. " +
  "Envíe NO si no ha recibido su tarjeta prepagada." +
  "Envíe AYUDA para repetir este mensaje.";

const helpText = {
  en: helpTextEn,
  es: helpTextEs,
};

const recvTextEn = "Text DONE to opt-out of receiving any more messages. ";
const recvTextEs = "Envíe DONE para dejar de recibir más mensajes.";
const recvText = {
  en: recvTextEn,
  es: recvTextEs,
};

const notRecvTextEn = "We'll check again in a week.";
const notRecvTextEs = "Lo comprobaremos de nuevo en una semana.";
const notRecvText = {
  en: notRecvTextEn,
  es: notRecvTextEs,
};

const optOutTextEn =
  "You have chosen to stop receiving automated messages form Baltimore Responds";
const optOutTextEs =
  "Ha decidido dejar de recibir mensajes automáticos de Baltimore Responds.";
const optOutText = {
  en: optOutTextEn,
  es: optOutTextEs,
};

const errorTextEn = "Invalid Command: " + helpTextEn;
const errorTextEs = "Comando inválido: " + helpTextEs;
const errorText = {
  en: errorTextEn,
  es: errorTextEs,
};
const messageType = "TRANSACTIONAL";

exports.handler = async (event) => {
  let error = null;
  const pool = new pg.Pool();
  const client = await pool.connect();
  const nowStr = new Date().toISOString();
  console.debug("length", event.Records.length);
  let count = 0;
  for (const record of event.Records) {
    const { body } = record;
    let commiting = false;
    try {
      const message = JSON.parse(JSON.parse(body).Message);
      const userPhone = message.originationNumber;
      const pinpointPhone = message.destinationNumber;
      const userMessage = message.messageBody.toLowerCase().trim();
      const EndpointId = userPhone.substring(1);
      console.debug(count, message);
      const getEndpointParamsDev = {
        ApplicationId: pinpointAppDev,
        EndpointId,
      };
      const getEndpointParamsProd = {
        ApplicationId: pinpointAppProd,
        EndpointId,
      };
      let endpoint = null;
      let stage = "dev";
      try {
        // try dev
        endpoint = await pp.getEndpoint(getEndpointParamsDev).promise();
      } catch {
        // do nothing
      }
      if (endpoint === null) {
        // try prod, if not in dev
        try {
          endpoint = await pp.getEndpoint(getEndpointParamsProd).promise();
          stage = "prod";
        } catch {
          // do nothing
        }
      }
      // if no endpoint is found in either project, throw here to stop the rest of the handler from operating
      if (endpoint === null) {
        throw Error(
          "The message came from an endpoint not registered to any application."
        );
      }
      if (stage !== process.env.STAGE) {
        // not in the right stage, leave
        throw Error("Message not from the correct stage. Skipping...");
      }
      const { UserAttributes } = endpoint.EndpointResponse.User;
      const { Attributes } = endpoint.EndpointResponse;
      console.log("Attributes", Attributes);
      console.log("UserAttributes", UserAttributes);
      let appId;
      if (Attributes.Application[0]) {
        appId = Attributes.Application[0];
      } else {
        throw Error("Missing Applicant ID");
      }
      const language = UserAttributes.Language
        ? UserAttributes.Language[0]
        : "en";

      if (recvKeyword.includes(userMessage)) {
        console.log("handling recv");
        const updateParams = {
          ApplicationId: pinpointApp[stage],
          EndpointId,
          EndpointRequest: {
            Address: userPhone,
            ChannelType: "SMS",
            Attributes: {
              Delivery: ["true"],
            },
          },
        };
        await pp.updateEndpoint(updateParams).promise();

        // NOTE: Get application
        const oldApp = await pool.query(
          "SELECT * FROM app_ccf_application WHERE application_id = $1 AND status='sent_for_payment'",
          [appId]
        );
        console.log(oldApp.rows[0]);

        // NOTE: get old status
        const oldStatus = await pool.query(
          "SELECT * FROM app_ccf_smscheckstatus WHERE application_id = $1 AND status='waiting'",
          [appId]
        );
        console.log(oldStatus.rows);

        await client.query("BEGIN");
        commiting = true;
        // NOTE: update current sms status
        const updateStatus = await client.query(
          "UPDATE app_ccf_smscheckstatus SET status='received' WHERE application_id = $1 AND status='waiting'",
          [appId]
        );
        console.log(updateStatus);

        // NOTE: create entry for sms status update
        let queryString =
          "INSERT INTO app_ccf_smscheckstatuschangelog(application_id,old_status,new_status,update_dt) VALUES($1,$2,$3,$4)";
        const insertStatusLog = await client.query(queryString, [
          appId,
          oldStatus.rows[0].status,
          "received",
          nowStr,
        ]);
        console.log(insertStatusLog);

        // NOTE: update application status to payment_confirmed
        const insertStatus = await client.query(
          "UPDATE app_ccf_application SET status='payment_confirmed', status_last_modified = $1 WHERE application_id = $2 AND status='sent_for_payment'",
          [nowStr, appId]
        );
        console.log(insertStatus);
        await client.query("COMMIT");
        commiting = false;

        // NOTE: Get new application
        const newApp = await pool.query(
          "SELECT * FROM app_ccf_application WHERE application_id = $1 AND status='payment_confirmed'",
          [appId]
        );
        console.log(newApp.rows[0]);

        await client.query("BEGIN");
        commiting = true;
        // NOTE: create application update entry
        queryString =
          "INSERT INTO staff_adminaction(admin_user,application_id,action_type,initial_app_json,final_app_json,action_dt) VALUES($1,$2,$3,$4,$5,$6)";
        const insertAdminActionStatusLog = await pool.query(queryString, [
          "lambda",
          appId,
          "lambda",
          oldApp.rows[0],
          newApp.rows[0],
          nowStr,
        ]);
        console.log(insertAdminActionStatusLog);

        queryString =
          "INSERT INTO app_ccf_statusupdate(status, update_dt, application_id) VALUES($1,$2,$3)";
        const insertUpdate = await client.query(queryString, [
          "payment_confirmed",
          nowStr,
          appId,
        ]);
        console.log(insertUpdate);
        await client.query("COMMIT");
        commiting = false;

        const sendParams = {
          ApplicationId: pinpointApp[stage],
          MessageRequest: {
            Addresses: {
              [userPhone]: {
                ChannelType: "SMS",
              },
            },
            MessageConfiguration: {
              SMSMessage: {
                Body: recvText[language],
                MessageType: messageType,
                OriginationNumber: pinpointPhone,
              },
            },
          },
        };
        const response = await pp.sendMessages(sendParams).promise();
        console.log(response.MessageResponse);
        console.log(JSON.stringify(response.MessageResponse, null, 2));
      } else if (helpKeyword.includes(userMessage)) {
        console.log("handling help");
        const sendParams = {
          ApplicationId: pinpointApp[stage],
          MessageRequest: {
            Addresses: {
              [userPhone]: {
                ChannelType: "SMS",
              },
            },
            MessageConfiguration: {
              SMSMessage: {
                Body: helpText[language],
                MessageType: messageType,
                OriginationNumber: pinpointPhone,
              },
            },
          },
        };
        const response = await pp.sendMessages(sendParams).promise();
        console.log(response.MessageResponse);
        console.log(JSON.stringify(response.MessageResponse, null, 2));
      } else if (optOutKeyword.includes(userMessage)) {
        console.log("handling optout");
        const updateParams = {
          ApplicationId: pinpointApp[stage],
          EndpointId,
          EndpointRequest: {
            Address: userPhone,
            ChannelType: "SMS",
            OptOut: "ALL",
          },
        };
        await pp.updateEndpoint(updateParams).promise();
        console.log(
          "Successfully changed the opt status of endpoint ID ",
          userPhone.substring(1),
          "optout"
        );
        const sendParams = {
          ApplicationId: pinpointApp[stage],
          MessageRequest: {
            Addresses: {
              [userPhone]: {
                ChannelType: "SMS",
              },
            },
            MessageConfiguration: {
              SMSMessage: {
                Body: optOutText[language],
                MessageType: messageType,
                OriginationNumber: pinpointPhone,
              },
            },
          },
        };
        const response = await pp.sendMessages(sendParams).promise();
        console.log(response.MessageResponse);
        console.log(JSON.stringify(response.MessageResponse, null, 2));
      } else if (userMessage.includes(notRecvKeyword)) {
        console.log("handling not recv");
        const sendParams = {
          ApplicationId: pinpointApp[stage],
          MessageRequest: {
            Addresses: {
              [userPhone]: {
                ChannelType: "SMS",
              },
            },
            MessageConfiguration: {
              SMSMessage: {
                Body: notRecvText[language],
                MessageType: messageType,
                OriginationNumber: pinpointPhone,
              },
            },
          },
        };
        const response = await pp.sendMessages(sendParams).promise();
        console.log(response.MessageResponse);
        console.log(JSON.stringify(response.MessageResponse, null, 2));
      } else {
        // bad text state
        const sendParams = {
          ApplicationId: pinpointApp[stage],
          MessageRequest: {
            Addresses: {
              [userPhone]: {
                ChannelType: "SMS",
              },
            },
            MessageConfiguration: {
              SMSMessage: {
                Body: `${errorText[language]}`,
                MessageType: messageType,
                OriginationNumber: pinpointPhone,
              },
            },
          },
        };
        const response = await pp.sendMessages(sendParams).promise();
        console.log(response.MessageResponse);
        console.log(JSON.stringify(response.MessageResponse, null, 2));
      }
    } catch (err) {
      if (commiting) {
        console.info("Mid-commit interrupt.");
        await client.query("ROLLBACK");
      }
      console.error(err);
    }
    count++;
  }
  if (client && client.release) {
    console.info("Release");
    await client.release();
  }
  if (pool && pool.end) {
    console.info("End");
    await pool.end();
  }
  return {};
};
