"use strict";
const AWS = require("aws-sdk");
const AWSXRay = require("aws-xray-sdk");
const pg = AWSXRay.capturePostgres(require("pg"));
const pp = AWSXRay.captureAWSClient(
  new AWS.Pinpoint({ region: process.env.region })
);

// Configure the context missing strategy to do nothing
// AWSXRay.setContextMissingStrategy(() => {});

const pinpointAppDev = process.env.projectIdDev;
const pinpointAppProd = process.env.projectIdProd;
const pinpointApp = {
  dev: pinpointAppDev,
  prod: pinpointAppProd,
};
const originationNumber = process.env.originationNumber;

// This message is spread across multiple lines for improved readability.
const messageEn =
  "Baltimore Responds: We will send weekly messages checking for " +
  "delivery of your Akimbo Now Promo Mastercard. Reply DONE to " +
  "opt-out of receiving text messages to check on prepaid card " +
  "delivery. Reply HELP to get a list of commands.";
const messageEs =
  "Baltimore Responds: Enviaremos mensajes semanales para verificar " +
  "la entrega de su tarjeta Mastercard de promoción Akimbo. Responda " +
  "DONE para terminar de recibir mensajes de texto comprobando la " +
  "entrega de la tarjeta prepagada. Envíe AYUDA para recibir una lista" +
  "de comandos.";
const message = {
  en: messageEn,
  es: messageEs,
};

var messageType = "PROMOTIONAL";

exports.handler = async (event) => {
  const pool = new pg.Pool({});
  const client = await pool.connect();
  try {
    console.log("Received event:", event);
    let userPhone = event.destinationNumber;
    if (userPhone.length == 10) {
      userPhone = "+1" + userPhone;
    }
    // TODO: collect phone numbers not able to receive text messages and send to balt-prom
    let validateParams = {
      NumberValidateRequest: {
        IsoCountryCode: "US",
        PhoneNumber: userPhone,
      },
    };
    const {
      firstName,
      lastName,
      source,
      received,
      language,
      appId,
      stage,
      ...rest
    } = event;
    const validated = await pp.phoneNumberValidate(validateParams).promise();
    console.log(validated);
    if (
      validated["NumberValidateResponse"]["PhoneTypeCode"] == 0 ||
      validated["NumberValidateResponse"]["PhoneTypeCode"] == 2 ||
      validated["NumberValidateResponse"]["PhoneTypeCode"] == 5
    ) {
      console.log("Creating endpoint:", validated);
      // createEndpoint(validated, event.firstName, event.lastName, event.source);
      userPhone =
        validated["NumberValidateResponse"]["CleansedPhoneNumberE164"];
      const endpointId = validated["NumberValidateResponse"][
        "CleansedPhoneNumberE164"
      ].substring(1);

      // NOTE: Check that the endpoint already exists
      // if yes, then return immediately with no error
      // if no, then continue
      const getEndpointParams = {
        ApplicationId: pinpointApp[stage],
        EndpointId: endpointId,
      };
      let endpoint = null;
      try {
        endpoint = await pp.getEndpoint(getEndpointParams).promise();
      } catch {
        // do nothing
      }
      if (endpoint !== null) {
        console.log(
          `The endpoint ${endpointId} already exists in the system. Returning early.`
        );
        await client.release();
        await pool.end();
        return {
          statusCode: 200,
        };
      }

      const createParams = {
        ApplicationId: pinpointApp[stage],
        // The Endpoint ID is equal to the cleansed phone number minus the leading
        // plus sign. This makes it easier to easily update the endpoint later.
        EndpointId: endpointId,
        EndpointRequest: {
          ChannelType: "SMS",
          Address: userPhone,
          // OptOut is set to ALL (that is, endpoint is opted out of all messages)
          // because the recipient hasn't confirmed their subscription at this
          // point. When they confirm, a different Lambda function changes this
          // value to NONE (not opted out).
          OptOut: "ALL",
          Location: {
            PostalCode: validated["NumberValidateResponse"]["ZipCode"],
            City: validated["NumberValidateResponse"]["City"],
            Country: validated["NumberValidateResponse"]["CountryCodeIso2"],
          },
          Demographic: {
            Timezone: validated["NumberValidateResponse"]["Timezone"],
          },
          Attributes: {
            Source: [source],
            Delivery: [received],
            Application: [appId],
          },
          User: {
            UserAttributes: {
              FirstName: [firstName],
              LastName: [lastName],
              Language: [language],
            },
          },
        },
      };
      const created = await pp.updateEndpoint(createParams).promise();
      console.log(created);
      // sendConfirmation(destinationNumber);

      await client.query("BEGIN");
      let queryString =
        "INSERT INTO app_ccf_smscheckstatus(application_id, phone_number, status) VALUES($1,$2,$3) ON CONFLICT (application_id) DO NOTHING";
      const insertStatus = await client.query(queryString, [
        appId,
        userPhone.slice(2),
        "waiting",
      ]);
      console.log(insertStatus);

      queryString =
        "INSERT INTO app_ccf_smscheckstatuschangelog(application_id, old_status, new_status,update_dt) VALUES($1,$2,$3,$4)";
      const insertStatusLog = await client.query(queryString, [
        appId,
        "waiting",
        "waiting",
        new Date().toISOString(),
      ]);
      console.log(insertStatusLog);
      await client.query("COMMIT");
      const updateParams = {
        ApplicationId: pinpointApp[stage],
        EndpointId: endpointId,
        EndpointRequest: {
          Address: userPhone,
          ChannelType: "SMS",
          OptOut: "NONE",
        },
      };
      await pp.updateEndpoint(updateParams).promise();
      console.log("Successfully Opted In", userPhone.substring(1));

      const sendParams = {
        ApplicationId: pinpointApp[stage],
        MessageRequest: {
          Addresses: {
            [userPhone]: {
              ChannelType: "SMS",
            },
          },
          MessageConfiguration: {
            SMSMessage: {
              Body: message[language],
              MessageType: messageType,
              OriginationNumber: originationNumber,
            },
          },
        },
      };
      const response = await pp.sendMessages(sendParams).promise();
      console.log(response.MessageResponse);
      console.log(JSON.stringify(response.MessageResponse, null, 2));
      await client.release();
      await pool.end();
      return {
        statusCode: 200,
      };
    } else {
      await client.release();
      await pool.end();
      console.error(
        "Received a phone number that isn't capable of receiving " +
          "SMS messages. No endpoint created."
      );
      return Error(
        "Received a phone number that isn't capable of receiving " +
          "SMS messages. No endpoint created."
      );
    }
  } catch (error) {
    await client.query("ROLLBACK");
    await client.release();
    await pool.end();
    console.error(error, error.stack);
    return {
      statusCode: 500,
    };
  }
};
