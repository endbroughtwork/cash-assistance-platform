const AWS = require("aws-sdk");
const AWSXRay = require("aws-xray-sdk");
// const pg = AWSXRay.capturePostgres(require("pg"));
const pp = AWSXRay.captureAWSClient(
  new AWS.Pinpoint({ region: process.env.region })
);

const pinpointAppDev = process.env.projectIdDev;
const pinpointAppProd = process.env.projectIdProd;
const pinpointApp = {
  dev: pinpointAppDev,
  prod: pinpointAppProd,
};
const OriginationNumber = process.env.originationNumber;
const messageType = "PROMOTIONAL";

const msgTextEn =
  "Baltimore Responds has rejected your assistance application. " +
  "If you would like to contest this decision, please contact " +
  "the community organization that gave you your access code.";
const msgTextEs =
  "Baltimore Responds ha rechazado su solicitud de asistencia. " +
  "Si desea impugnar esta decisión, por favor, póngase en contacto " +
  "con la organización de la comunidad que le dio el código de acceso.";
const msgText = {
  en: msgTextEn,
  es: msgTextEs,
};

exports.handler = async (event) => {
  console.log("Received event:", event);
  const returnObj = { statusCode: 200 };
  try {
    let userPhone = event.destinationNumber;
    const { language, stage } = event;
    if (userPhone.length == 10) userPhone = "+1" + userPhone;
    const validateParams = {
      NumberValidateRequest: { IsoCountryCode: "US", PhoneNumber: userPhone },
    };
    const validated = await pp.phoneNumberValidate(validateParams).promise();
    console.log(validated);
    if (
      validated["NumberValidateResponse"]["PhoneTypeCode"] == 0 ||
      validated["NumberValidateResponse"]["PhoneTypeCode"] == 2 ||
      validated["NumberValidateResponse"]["PhoneTypeCode"] == 5
    ) {
      // createEndpoint(validated, event.firstName, event.lastName, event.source);
      userPhone =
        validated["NumberValidateResponse"]["CleansedPhoneNumberE164"];
      const endpointId = validated["NumberValidateResponse"][
        "CleansedPhoneNumberE164"
      ].substring(1);
      const sendParams = {
        ApplicationId: pinpointApp[stage],
        MessageRequest: {
          Addresses: {
            [userPhone]: {
              ChannelType: "SMS",
            },
          },
          MessageConfiguration: {
            SMSMessage: {
              Body: `${msgText[language]}`,
              MessageType: messageType,
              OriginationNumber,
            },
          },
        },
      };
      console.log("Sending message:", sendParams);
      const response = await pp.sendMessages(sendParams).promise();
      console.log(response.MessageResponse);
      console.log(JSON.stringify(response.MessageResponse, null, 2));
      return returnObj;
    }
    returnObj.statusCode = 500;
    returnObj.message =
      "Could not send a text message: Failed phone number validation.";
    return returnObj;
  } catch (err) {
    console.error(err);
    returnObj.statusCode = 500;
    returnObj.message = `Could not send a text message: ${err.message}`;
  }
};
