variable "project_code" {
  type    = string
  default = "balt"
}

variable "region" {
  type    = string
  default = "us-east-1"
}

variable "project_name" {
  type    = string
  default = "Baltimore Responds"
}

variable "project_account" {
  type    = string
  default = "785030614018"
}

variable "tld" {
  type    = string
  default = "org"
}

variable "domain_name" {
  type    = string
  default = "baltimoreresponds"
}

data "aws_availability_zones" "available" {}

variable "tf_s3_bucket" {
  type        = string
  default     = "com-baltimores-promise-lockbox"
  description = "S3 bucket to maintain the shared Terraform state"
}

# AMI for the most recent ECS-optimized EC2 instance
data "aws_ami" "ecs_optimized" {
  most_recent = "true"

  filter {
    name   = "name"
    values = ["*amazon-ecs-optimized"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["amazon"]
}
