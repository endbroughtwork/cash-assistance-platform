resource "aws_pinpoint_app" "app" {
  name = "${var.app_name}-${var.stage_code}"

  quiet_time {
    end   = "09:00"
    start = "19:00"
  }

  limits {
    maximum_duration    = 10800
    messages_per_second = 20000
  }
}

resource "aws_pinpoint_sms_channel" "sms" {
  application_id = aws_pinpoint_app.app.application_id
}
