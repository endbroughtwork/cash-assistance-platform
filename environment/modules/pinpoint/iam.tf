resource "aws_iam_role" "pinpoint_reg_role" {
  name               = "pinpoint-reg-role-${var.stage_code}"
  assume_role_policy = data.aws_iam_policy_document.reg_assume_role.json
}

resource "aws_iam_policy" "pinpoint_reg_policy" {
  name        = "pinpoint-reg-policy-${var.stage_code}"
  path        = "/"
  description = "Allows Lambda to register users on SMS response"

  policy = data.aws_iam_policy_document.pinpoint_reg_policy_doc.json
}

resource "aws_iam_role_policy_attachment" "pinpoint_reg_attach" {
  role       = aws_iam_role.pinpoint_reg_role.name
  policy_arn = aws_iam_policy.pinpoint_reg_policy.arn
}

data "aws_iam_policy_document" "reg_assume_role" {
  policy_id = "sns_assume_role"

  statement {
    actions = [
      "sts:AssumeRole"
    ]

    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "pinpoint_reg_policy_doc" {
  policy_id = "pinpoint-reg-${var.stage_code}"

  statement {
    actions = [
      "mobiletargeting:*",
    ]

    effect = "Allow"

    resources = [
      "*"
    ]

    sid = "PinpointAccess"
  }

  statement {
    actions = [
      "sns:*"
    ]

    effect = "Allow"

    resources = [
      "*",
    ]

    sid = "PinpointAnySNStopic"
  }
}
