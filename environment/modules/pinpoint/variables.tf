variable "region" {
  default = "us-east-1"
}

variable "app_name" {}

variable "account_id" {}

variable "stage_code" {}

variable "stage_name" {}
