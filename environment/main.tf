module "staging" {
  source     = "./staging"
  account_id = var.project_account
  region     = var.region
}

module "production" {
  source     = "./production"
  account_id = var.project_account
  region     = var.region
}

resource "aws_acm_certificate" "cert" {
  domain_name = "${var.domain_name}.${var.tld}"

  subject_alternative_names = [
    "*.dev.${var.domain_name}.${var.tld}",
    "*.${var.domain_name}.${var.tld}"
  ]

  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name      = "cert-${var.project_code}"
    Terraform = true
  }
}

resource "aws_route53_record" "cert_validation_0" {
  name    = aws_acm_certificate.cert.domain_validation_options[0].resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options[0].resource_record_type
  zone_id = data.aws_route53_zone.main.id
  records = [aws_acm_certificate.cert.domain_validation_options[0].resource_record_value]
  ttl     = 60
}

resource "aws_route53_record" "cert_validation_1" {
  name    = aws_acm_certificate.cert.domain_validation_options[1].resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options[1].resource_record_type
  zone_id = data.aws_route53_zone.main.id
  records = [aws_acm_certificate.cert.domain_validation_options[1].resource_record_value]
  ttl     = 60
}


data "aws_route53_zone" "main" {
  name         = "${var.domain_name}.${var.tld}."
  private_zone = false
}

resource "aws_sns_sms_preferences" "update_sms_prefs" {
  monthly_spend_limit          = 20
  delivery_status_iam_role_arn = aws_iam_role.sms_logging_role.arn
  default_sms_type             = "Transactional"
  usage_report_s3_bucket       = "balt-prom-sms-logging"
}

resource "aws_iam_role" "sms_logging_role" {
  name               = "sms_logging-role"
  assume_role_policy = data.aws_iam_policy_document.sns_assume_role.json
}

resource "aws_iam_policy" "sms_logging_policy" {
  name        = "sms_logging_policy-role"
  path        = "/"
  description = "Allows SNS to log SMS message delivery"

  policy = data.aws_iam_policy_document.sms_logging_policy.json
}

resource "aws_iam_role_policy_attachment" "sns_sms_logging_attach" {
  role       = aws_iam_role.sms_logging_role.name
  policy_arn = aws_iam_policy.sms_logging_policy.arn
}

data "aws_iam_policy_document" "sms_logging_policy" {
  policy_id = "allow_sms_logging"

  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:PutMetricFilter",
      "logs:PutRetentionPolicy"
    ]

    effect = "Allow"

    resources = [
      "*",
    ]

    sid = ""
  }
}

data "aws_iam_policy_document" "sns_assume_role" {
  policy_id = "sns_assume_role"

  statement {
    actions = [
      "sts:AssumeRole"
    ]

    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["sns.amazonaws.com"]
    }
  }
}
