module "pinpoint" {
  source     = "../modules/pinpoint"
  app_name   = "ccf"
  account_id = var.account_id
  region     = var.region
  stage_code = var.stage_code
  stage_name = var.stage_name
}

resource "aws_sns_topic" "sms_registration" {
  name = "${var.project_code}-sms-register-${var.stage_code}"
  # kms_master_key_id = "alias/aws/sns"
}

resource "aws_sns_topic" "sms_delivery_check" {
  name = "${var.project_code}-sms-delivery-check-${var.stage_code}"
  # kms_master_key_id = "alias/aws/sns"
}
