variable "project_code" {
  type    = string
  default = "balt"
}

variable "stage_code" {
  default = "prod"
}

variable "stage_name" {
  default = "Production"
}

variable "region" {
  type    = string
  default = "us-east-1"
}

variable "project_name" {
  type    = string
  default = "COVID-19 Emergency Assistance Program"
}

variable "account_id" {
  type    = string
  default = "785030614018"
}

data "aws_availability_zones" "available" {}
