terraform {
  backend "s3" {
    bucket = "com-baltimores-promise-lockbox"
    key    = "resources.tfstate"
    region = "us-east-1"
    profile = "balt"
  }
}

terraform {
  required_providers {
    aws = {
      version = "~> 2.0"
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region = var.region
  profile = var.project_code
}
