# -*- coding: utf-8 -*-
"""
    cash-assistance-platform.staff.views
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Staff applications views

    :copyright: (c) 2021 by YOUR_NAME.
    :license: LICENSE_NAME, see LICENSE for more details.
"""
import csv
import io
import logging
import os
import pprint
import urllib.parse
from collections import OrderedDict
from datetime import datetime
from datetime import timedelta
from datetime import timezone

from django.utils.translation import ugettext_lazy as _
import unidecode
from backoff import expo
from backoff import on_exception
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import logout
from django.contrib.auth.views import PasswordChangeView
from django.db import transaction
from django.db.models import Q
from django.db.models.functions import Lower
from django.forms import Textarea
from django.forms.models import model_to_dict
from django.forms.models import modelform_factory
from django.http import FileResponse
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.shortcuts import render
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import TemplateView
from django.views.generic import UpdateView
from django.views.generic import View
from django.views.generic.base import ContextMixin
from django.views.generic.edit import CreateView
from django_filters import ChoiceFilter
from django_filters import FilterSet
from django_filters import ModelChoiceFilter
from django_filters import rest_framework as rest_filters
from django_filters.views import FilterView
from django_tables2 import MultiTableMixin
from ratelimit import limits
from ratelimit import RateLimitException
from rest_framework import generics
from rest_framework.views import APIView

from .forms import ApprovedApplicationTable
from .forms import PreapprovedAddressGenerateForm
from .forms import VoucherCodeGenerateForm
from .mixins import StaffRequiredMixin
from .mixins import SuperUserRequiredMixin
from .models import AdminAction
from .utils import generate_voucher_codes
from .utils import get_archived_payment_actions
from .utils import import_voucher_codes
from .utils import invalidate_voucher_codes_with_code_list
from .utils import upload_payment_action_archive
from app_ccf import common
from app_ccf import notification
from app_ccf.config import get_config
from app_ccf.models import Application
from app_ccf.models import DistributorXRef
from app_ccf.models import PreapprovedAddress
from app_ccf.models import VoucherCode
from app_ccf.models import VoucherCodeBatch
#  from .utils import deregister_card
#  from .utils import load_card
#  from .utils import send_applicant_info
#  from .utils import send_card_info
#  from app_ccf import text_messages
#  from django_tables2 import SingleTableView
#  from django.contrib.auth.mixins import LoginRequiredMixin
#  from django.contrib.auth.models import User
#  from django.core.files import File
#  from django.shortcuts import get_object_or_404
#  from django.template.loader import render_to_string
#  from django.urls import reverse_lazy
#  from django_filters import CharFilter, DateFromToRangeFilter
#  from django_filters.widgets import RangeWidget
#  from .utils import invalidate_voucher_codes_with_campaign

PPRINTER = pprint.PrettyPrinter(indent=2)
LOGGER = logging.getLogger(__name__)


def ccf_bad_request_view(request):
    """
    shown on bad http request
    """
    messages.error(request, 'Bad request.')
    return render(
        request,
        'staff/40x.html',
        status=400
    )


def ccf_permssion_denied_view(request):
    """
    shown when you lack permission for a page
    """
    messages.error(request, 'You are not allowed to view this page.')
    return render(
        request,
        'staff/40x.html',
        status=403
    )


def ccf_not_found_view(request):
    """
    shown when a page does not exist
    """
    messages.error(request, 'Page not found.')
    return render(
        request,
        'staff/40x.html',
        status=404
    )


def staff_logout_view(request):
    """
    shown on logout
    """
    logout(request)
    messages.info(request, 'Logged out successfully')
    return redirect('staff:index')


# # # # # # # # # # # # # # # # # # # # # # # #
# Staff-required CCF application related views
# # # # # # # # # # # # # # # # # # # # # # # #

class ApplicationFilter(FilterSet):
    """Application filter used when loading Application table"""
    class Meta:
        """
        Meta
        """
        model = Application
        fields = {'status': ['exact'],
                  'first_name': ['unaccent__icontains'],
                  'last_name': ['unaccent__icontains'],
                  'phone_number': ['icontains'],
                  'vouchercode_str': ['icontains'],
                  'email': ['icontains'],
                  'addr1': ['icontains'],
                  'addr2': ['icontains'],
                  'city': ['iexact'],
                  'state': ['iexact'],
                  }


class IndexView(StaffRequiredMixin, TemplateView):
    """
    Base page
    """
    template_name = 'staff/index.html'


def index_view(
    #  request
):
    """
    This view is a FAKE login page that a potential hacker may use.
    """
    # TODO: Additional config needed for notifying admins of login attempts.
    # TODO: customization of login pages.
    return HttpResponseRedirect(reverse('admin_honeypot:login',))


class PaginatedFilterViewMixin(View):
    """
    TBD
    """

    def get_context_data(self, **kwargs):
        """
        context data for the html template
        """
        context = super().get_context_data(**kwargs)
        # Get filter params if in URL querystring
        if self.request.GET:
            querystring = self.request.GET.copy()
            if self.request.GET.get('page'):
                del querystring['page']
            context['params_GET'] = querystring.urlencode()
        return context


class AutoProcessApplicationsView(StaffRequiredMixin, TemplateView):
    """
    TBD
    """

    def post(self, request, *args, **kwargs):
        """
        initiate application update
        """
        common.auto_update_application_statuses()
        return HttpResponseRedirect(reverse('staff:payments'))


class ApplicationListView(StaffRequiredMixin, PaginatedFilterViewMixin, ListView):
    """
    Show applications in a table, filterable
    """
    model = Application
    template_name = "staff/applications/application_list.html"
    ordering = ["last_name", "first_name", "-submitted_date"]
    paginate_by = 10

    def get_queryset(self):
        """
        get entries
        """
        queryset = ApplicationFilter(
            self.request.GET,
            queryset=self.model.objects.all().order_by(*self.ordering)
        ).qs
        return queryset

    def get_context_data(self, **kwargs):
        """
        context data for use in the html template
        """
        context = super().get_context_data(**kwargs)
        context['filter'] = ApplicationFilter(
            self.request.GET, queryset=self.get_queryset())
        return context


class ApplicationDetailView(StaffRequiredMixin, DetailView):
    """
    show application information for a single person in detail with changetsets
    """
    model = Application
    template_name = "staff/applications/application_details.html"

    def get_context_data(self, **kwargs):
        """
        context data for the html template
        """
        context = super().get_context_data(**kwargs)
        context['code'] = self.object.vouchercode
        return context


class LoginRequiredAfterPasswordChange(PasswordChangeView):
    """
    TBD
    """
    @property
    def success_url(self):
        """
        TBD
        """
        messages.success(
            self.request,
            'You have successfully updated you password.'
        )
        return reverse('staff:index')


# # # # # # # # # # # # # # # # # # # # # # # #
# Superuser-required views
# # # # # # # # # # # # # # # # # # # # # # # #


class SuperUserAccessedView(SuperUserRequiredMixin, TemplateView):
    """
    TBD
    """
    template_name = None


class ApplicationUpdateView(StaffRequiredMixin, UpdateView):
    """
    page contains a form that allows edits to an individual applicant
    """
    model = Application
    template_name = "staff/applications/application_update.html"
    form_class = modelform_factory(
        Application,
        fields=['first_name', 'last_name', 'phone_number', 'email', 'addr1',
                'addr2', 'city', 'state', 'zip_code', 'status', 'note'],
        widgets={'note': Textarea(attrs={'cols': 30,
                                         'rows': 3,
                                         'style': 'vertical-align: top'})})

    def get_form(self, *args, **kwargs):
        form = super(ApplicationUpdateView, self).get_form(*args, **kwargs)
        form.fields['note'].required = True
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_success_url(self):
        return reverse('staff:application-detail', kwargs={'pk': self.object.pk})

    @on_exception(expo, RateLimitException, max_tries=2)
    @limits(calls=5, period=1)
    @transaction.atomic
    def form_valid(self, form):
        if form.has_changed():
            initial = model_to_dict(self.get_object())
            final = model_to_dict(self.object)
            admin_action = AdminAction(
                admin_user=self.request.user.username,
                application=self.object,
                action_type=AdminAction.AdminActionType.MANUAL_EDIT,
                initial_app_json=initial,
                final_app_json=final
            )
            admin_action.save()
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        if self.object.status == Application.ApplicationStatus.REJECTED and \
                self.request.POST.get('send_rejection_notice'):
            notification.send_text(
                self.object, notification.TextType.REJECTION)
        return response


class PaymentsView(SuperUserRequiredMixin, MultiTableMixin, TemplateView):
    """
    Show applications and mark for payment.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = Application
        self.template_name = "staff/applications/payment_list.html"
        self.tables = [
            ApprovedApplicationTable(self.get_approved_list(), exclude=("application_id", "type_of_work",
                                                                        "payment_confirmed_reminder_sent",
                                                                        "age_range", "household_size",
                                                                        "household_income", "ethnicity",
                                                                        "gender", "language",
                                                                        "addr1", "addr2", "city", "state",
                                                                        "zip_code", "email", "usps_standardized",
                                                                        "signature", "vouchercode_str"))
        ]
        self.table_pagination = {
            "per_page": 50
        }

    def get_approved_list(self):
        return Application.objects.filter(
            status=Application.ApplicationStatus.APPROVED
        )

    def get_paid_out_list(self):
        return Application.objects.filter(
            Q(status=Application.ApplicationStatus.PAYMENT_CONFIRMED) |
            Q(status=Application.ApplicationStatus.SENT_FOR_PAYMENT)
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['app_status_list'] = Application.ApplicationStatus.choices
        paid_out_apps = self.get_paid_out_list()
        approved_apps = self.get_approved_list()

        self.tables = [ApprovedApplicationTable(approved_apps, exclude=("application_id", "type_of_work",
                                                                        "payment_confirmed_reminder_sent",
                                                                        "age_range", "household_size",
                                                                        "household_income", "ethnicity",
                                                                        "gender", "language",
                                                                        "status_last_modified",
                                                                        "addr1", "addr2", "city", "state",
                                                                        "zip_code", "email", "usps_standardized",
                                                                        "signature", "vouchercode_str"))
                       ]
        context['paid_out_apps'] = paid_out_apps
        context['paid_out_ids'] = [app.application_id.hex for app in paid_out_apps]
        context['num_paid_out'] = len(paid_out_apps)

        context['approved_apps'] = approved_apps
        context['approved_ids'] = [app.application_id.hex for app in approved_apps]
        context['num_approved'] = len(approved_apps)
        return context
    #  def post(self, request, *args, **kwargs):
    #      approved_apps = self.get_approved_list()
    #      approved_ids = [str(app.application_id) for app in approved_apps]
    #      PPRINTER.pprint(approved_ids)
    #
    #      if len(approved_ids) == 0:
    #          return HttpResponse(status=400)
        #  return HttpResponse(status=200)
        #  app_ids = request.POST.getlist('apps[]')
        #  for app in apps_to_pay:
        #      try:
        #          voucher = VoucherCode.objects.get(code=app.vouchercode_str)
        #          amount = voucher.batch.base_amount + voucher.added_amount
        #          distributor_id = str(voucher.batch.affiliate.distributor_id)
        #          respId = send_applicant_info(app, distributor_id)
        #          cardId = send_card_info(respId, distributor_id)
        #          deregister_card(cardId)
        #          load_card(cardId, amount, distributor_id)
        #          app.status = Application.ApplicationStatus.SENT_FOR_PAYMENT
        #          app.save()
        #          notification.send_text(app, notification.TextType.SENT_FOR_PAYMENT)
        #      except Exception as err:
        #          LOGGER.error('#PaymentStatusUpdateError: %s', err)
        #          # TODO: handle an error in the card, send to a list of failed apps, maybe?
        #  return HttpResponse(status=200)


class DownloadReportView(SuperUserRequiredMixin, APIView):
    def get_approved_list(self):
        return Application.objects.filter(
            status=Application.ApplicationStatus.APPROVED
        )

    def post(self, request, *args, **kwargs):
        try:
            apps_to_pay = self.get_approved_list()
            output = self.write_payments_csv(apps_to_pay)
            # NOTE: When a download is requested for APPROVED applications, we update
            # their status to SENT_FOR_PAYMENT. This is a bit of a hack while we
            # still need CSVs to be uploaded for payment manually.
            common.update_application_statuses(
                apps_to_pay, Application.ApplicationStatus.SENT_FOR_PAYMENT)
            dt_now = datetime.now(timezone(-timedelta(hours=5), "EST")).strftime("%Y%m%d")
            name = 'batchCardCreate_{}_957_OSI Baltimore.csv'.format(dt_now)
            upload_payment_action_archive(output, name)
        except Exception as err:
            LOGGER.error('#PaymentReportDownloadError: %s', err)
            resp = HttpResponse('Unknown Error')
            resp.status_code = 500
            return resp
        response = HttpResponse(output, content_type='text/csv')
        return response

    def write_payments_csv(self, applications):
        # TODO: Need to pre-load voucher codes because calling
        # VoucherCode.objects.get(code=application.voucher_code) on every
        # application is extremely slow
        voucher_code_amount_map = {
            voucher_code.code: {
                'load': voucher_code.added_amount + voucher_code.batch.base_amount,
                'dis_id': voucher_code.batch.affiliate.distributor_id
            }
            for voucher_code in VoucherCode.objects.filter(
                code__in=applications.values_list('vouchercode_str', flat=True))
        }
        rows = []
        for application in applications:
            load_amount = voucher_code_amount_map[application.vouchercode_str]['load']
            dis_id = voucher_code_amount_map[application.vouchercode_str]['dis_id']
            card_design_id = get_config()['usio_card_design_id_%s' % application.language]
            data = OrderedDict([('cardType', 'Incentive-Card'), ('cardCount', '1'),
                                ('shippingDestination', 'consumer'), ('firstName', application.first_name),
                                ('lastName', application.last_name), ('address', application.addr1),
                                ('address2', application.addr2), ('city', application.city),
                                ('state', application.state), ('zipCode', application.zip_code),
                                ('phone', application.phone_number.replace('+', '')),
                                ('email', ''),  # not sharing emails for privacy
                                ('loadAmount', str(load_amount)), ('loadNow', 'y'), ('distributorId', dis_id),
                                ('cardDesignId', card_design_id), ('giftMessage', ''),
                                ('giftSenderFirstName', ''), ('giftSenderLastName', ''),
                                ('giftVirtualDeliveryMethod', ''), ('expiresIn', ''), ('reportData1', ''),
                                ('reportData2', ''), ('reportData3', ''), ('Source', ''),
                                ('Shipping Method', '')
                                ])
            for key, value in data.items():
                data[key] = unidecode.unidecode(value)
            rows.append(data)
        fieldnames = rows[0].keys()
        output = io.StringIO()
        writer = csv.DictWriter(output, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(rows)
        print(output.getvalue())
        return output.getvalue().encode('utf-8')


class DownloadApplicationsView(SuperUserRequiredMixin, APIView):
    def get_list(self):
        return Application.objects.all()

    def get(self, request, *args, **kwargs):
        try:
            apps = self.get_list()
            output = self.write_application_csv(apps)
        except Exception as err:
            LOGGER.error('#ApplicationListDownloadError: %s', err)
            resp = HttpResponse('Unknown Error')
            resp.status_code = 500
            return resp
        response = HttpResponse(output, content_type='text/csv')
        dt_now = datetime.now(timezone(-timedelta(hours=5), "EST")).strftime("%Y%m%d_%H-%M")
        response['Content-Disposition'] = \
            'attachment; filename="application_export_{}.csv"'.format(dt_now)
        #  response.set_headers()
        return response

    def write_application_csv(self, applications):
        voucher_code_amount_map = {
            voucher_code.code: {
                'dis_id': voucher_code.batch.affiliate.distributor_id,
                'dis_name': voucher_code.batch.affiliate.name
            }
            for voucher_code in VoucherCode.objects.filter(
                code__in=applications.values_list('vouchercode_str', flat=True))
        }
        rows = []

        trs_tbl = {'\n': ' ', '\t': ' ', '\r': ' '}
        for application in applications:
            dis_name = voucher_code_amount_map[application.vouchercode_str]['dis_name']
            dis_id = voucher_code_amount_map[application.vouchercode_str]['dis_id']
            data = OrderedDict([('Name', '{}, {}'.format(application.last_name, application.first_name)),
                                ('Affiliate ID', dis_id), ('Affiliate Name', dis_name),
                                ('Submit Date', application.submitted_date.astimezone(
                                    timezone(offset=-timedelta(hours=5), name="EST")).strftime("%b %d, %Y %I:%M %p")
                                 ),
                                ('Last Updated', application.status_last_modified.astimezone(
                                    timezone(offset=-timedelta(hours=5), name="EST")).strftime("%b %d, %Y %I:%M %p")
                                 ),
                                ('status', application.status),
                                ('Note', application.note.translate(str.maketrans(trs_tbl))),
                                ('Code', application.vouchercode_str), ('phone', application.phone_number),
                                ('Address', application.get_full_address().translate(str.maketrans(trs_tbl))),
                                ])
            for key, value in data.items():
                data[key] = unidecode.unidecode(value)
            rows.append(data)
        fieldnames = rows[0].keys()
        output = io.StringIO()
        writer = csv.DictWriter(output, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(rows)
        return output.getvalue().encode('utf-8')


class PaymentsArchiveView(SuperUserRequiredMixin, TemplateView):
    """
    Show applications and mark for payment.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.template_name = "staff/applications/payment_action_archive.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = get_archived_payment_actions()
        return context


class VoucherCodeGenerateView(SuperUserRequiredMixin, CreateView, ListView, ContextMixin):
    template_name = "staff/voucher.html"
    form_class = VoucherCodeGenerateForm

    affiliates = DistributorXRef.objects.all()
    queryset = VoucherCodeBatch.objects.order_by('-created')
    paginate_by = 10

    def get_initial(self):
        initial = super().get_initial()
        initial['base_amount'] = get_config()['payment_amount']
        return initial

    def get_success_url(self):
        return '{}?{}'.format(
            reverse('staff:voucher-gen'),
            urllib.parse.urlencode({
                'success': True,
                'num_codes_generated': self.object.num_codes,
                'batch_id': self.object.id,
            })
        )

    def get_context_data(self, **kwargs):
        self.object_list = VoucherCodeBatch.objects.order_by('-created')
        context = super(VoucherCodeGenerateView,
                        self).get_context_data(**kwargs)

        context['affiliates'] = self.affiliates
        # Add vars for success alert if codes were just generated
        if (self.request.GET.get('success')) == str(True):
            context['success'] = True
            context['num_codes_generated'] = self.request.GET.get(
                'num_codes_generated')
            context['batch_id'] = self.request.GET.get(
                'batch_id')

        return context

    def form_valid(self, form):
        model = form.instance
        model.code_length = 9
        model.alphabet = 'abcdefghijkmnopqrstuvwxyz'
        model.admin_user = self.request.user.username

        #  for field, val in model_to_dict(model).items():
        #      LOGGER.debug(field + "=" + str(val))

        # Need to save before creating codes so the batch can be attached
        model.save()

        # Create codes and write to a temp file
        codes = generate_voucher_codes(
            model.num_codes, model.code_length, model.alphabet)
        filename = 'tmp.csv'
        with open(filename, 'w') as f:
            for code in codes:
                f.write('%s\n' % code)

        # Add codes to the database
        import_voucher_codes(filename, model)

        return super().form_valid(form)


class DownloadVoucherCodeBatchView(SuperUserRequiredMixin, TemplateView):
    template_name = "staff/voucher.html"

    def get(self, request, *args, **kwargs):
        batch_id = self.kwargs.get('batch_id')
        batch = VoucherCodeBatch.objects.get(id=batch_id)
        filename_raw = "codes_batch{id}_{campaign}_{ts}".format(
            id=batch_id,
            campaign=batch.campaign,
            ts=batch.created.strftime('%Y-%m-%d'))
        filename = slugify(filename_raw) + '.csv'  # clean filename
        with open(filename, 'w') as file:
            codes = batch.vouchercode_set.values_list('code', flat=True)
            file.writelines('\n'.join(codes))

        # Need 'rb' and not 'r' or you will get a very strange error
        return FileResponse(open(filename, 'rb'), filename=filename)


class VoucherCodeBatchInvalidateView(SuperUserRequiredMixin, TemplateView):
    template_name = "staff/voucher.html"

    def get(self, request, *args, **kwargs):
        batch_id = self.kwargs.get('batch_id')
        batch = VoucherCodeBatch.objects.get(id=batch_id)
        batch.batch_active = False
        voucher_codes = batch.vouchercode_set.values_list('code', flat=True)
        invalidate_voucher_codes_with_code_list(voucher_codes)
        batch.save()
        return HttpResponseRedirect(reverse('staff:voucher-gen'))


def get_choices(field):
    # We have to process this whole thing within a QuerySet because trying to
    # extract any values eagerly will get us a [ProgrammingError: relation
    # "app_ccf_vouchercodebatch" does not exist"]. The (field, field) part is to
    # pull out choice tuples in the form of (value, label) (see other usage in
    # ApplicationFilter above).
    return VoucherCodeBatch.objects.values_list(
        field, field).distinct().order_by(Lower(field))


class VoucherCodeFilter(FilterSet):
    batch__channel = ChoiceFilter(
        choices=get_choices('channel'),
        label='Channel')
    #  batch__affiliate = ChoiceFilter(
    #      choices=get_choices('affiliate'),
    #      label='Affiliate')
    batch__affiliate = ModelChoiceFilter(
        queryset=DistributorXRef.objects.all().values_list(flat=True),
        label='Affiliate',
    )
    batch__campaign = ChoiceFilter(
        choices=get_choices('campaign'),
        label='Campaign')

    class Meta:
        model = VoucherCode
        fields = {
            'code': ['icontains'],
        }


class VoucherCodeFilterAPI(rest_filters.FilterSet):
    batch__channel = ChoiceFilter(
        choices=get_choices('channel'),
        label='Channel')
    #  batch__affiliate = ChoiceFilter(
    #      choices=get_choices('affiliate'),
    #      label='Affiliate')
    batch__affiliate = ModelChoiceFilter(
        queryset=DistributorXRef.objects.all().values_list(flat=True),
        label='Affiliate',
    )
    batch__campaign = ChoiceFilter(
        choices=get_choices('campaign'),
        label='Campaign')

    class Meta:
        model = VoucherCode
        fields = {
            'code': ['icontains'],
        }


class VoucherCodeListView(StaffRequiredMixin, PaginatedFilterViewMixin, FilterView):
    template_name = "staff/voucher_list.html"
    queryset = VoucherCode.objects.filter(
        is_active=True).order_by('-batch__created')
    filterset_class = VoucherCodeFilter
    paginate_by = 50

    def get_context_data(self, **kwargs):
        context = super(VoucherCodeListView, self).get_context_data(**kwargs)
        context['code_list'] = ' '.join(
            [voucher_code.code for voucher_code in kwargs['object_list']])
        context['code_list_ids'] = [voucher_code.code for voucher_code in kwargs['object_list']]

        # Set 'empty' to True if the page load is not from an explicit search,
        # which we use to decide whether or not to show the "invalidate" button
        context['empty'] = dict(self.request.GET) == {}

        # Add vars for success alert if codes were just generated
        if (self.request.GET.get('success')) == str(True):
            context['success'] = True
            context['num_codes_invalidated'] = self.request.GET.get(
                'num_codes_invalidated')
        return context


class VoucherCodeListCreateApplicationRedirectView(SuperUserRequiredMixin, TemplateView):

    def get(self, request, *args, **kwargs):
        code = self.kwargs.get('code')
        if settings.APP_TYPE == 'STAFF':
            # Redirect to non-staff version of site
            url = 'https://{}/{}/'.format(
                self.request.get_host().replace('staff-', ''), code)
        else:  # demo or local
            url = reverse('language', kwargs={'accesscode': code})
        return HttpResponseRedirect(url)


class VoucherCodeListInvalidateView(SuperUserRequiredMixin, TemplateView):
    template_name = "staff/voucher_list.html"

    def post(self, request, *args, **kwargs):
        voucher_codes = request.POST.get('submit-invalidate').split()
        invalidate_voucher_codes_with_code_list(voucher_codes)
        return HttpResponseRedirect(
            '{}?{}'.format(
                reverse('staff:voucher-list'),
                urllib.parse.urlencode({
                    'success': True,
                    'num_codes_invalidated': len(voucher_codes),
                })
            ))


class VoucherCodeListDownloadView(SuperUserRequiredMixin, generics.ListAPIView):
    queryset = VoucherCode.objects.filter(is_active=True).order_by('-batch__created')
    filter_backends = (rest_filters.DjangoFilterBackend,)
    filterset_class = VoucherCodeFilterAPI

    def get(self, request, *args, **kwargs):
        try:
            vouchers = self.filter_queryset(self.queryset)
            LOGGER.info('Num vouchers: %d', len(vouchers))
            voucher_code_info_map = {
                voucher_code.code: {
                    'id': voucher_code.batch.affiliate.distributor_id,
                    'name': voucher_code.batch.affiliate.name,
                    'channel': voucher_code.batch.channel,
                    'campaign': voucher_code.batch.campaign,
                    'created': voucher_code.batch.created.astimezone(
                        tz=timezone(-timedelta(hours=5), name="EST")
                    ).strftime("%b %d, %Y %I:%M %p"),
                    'amount': voucher_code.batch.base_amount + voucher_code.added_amount,
                    'expires': voucher_code.batch.expiration_date.astimezone(
                        tz=timezone(-timedelta(hours=5), name="EST")
                    ).strftime("%b %d, %Y %I:%M %p"),
                }
                for voucher_code in vouchers
            }
            rows = []
            for voucher in vouchers:
                data = OrderedDict([('Code', voucher.code),
                                    ('Channel', voucher_code_info_map[voucher.code]['channel']),
                                    ('Affiliate ID', voucher_code_info_map[voucher.code]['id']),
                                    ('Affiliate', voucher_code_info_map[voucher.code]['name']),
                                    ('Campaign', voucher_code_info_map[voucher.code]['campaign']),
                                    ('Amount', str(voucher_code_info_map[voucher.code]['amount'])),
                                    ('Created', voucher_code_info_map[voucher.code]['created']),
                                    ('Expires', voucher_code_info_map[voucher.code]['expires']),
                                    ])
                for key, value in data.items():
                    data[key] = unidecode.unidecode(value)
                rows.append(data)
            LOGGER.info(PPRINTER.pformat(rows[0:5]))
            fieldnames = rows[0].keys()
            output = io.StringIO()
            writer = csv.DictWriter(output, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(rows)
            response = HttpResponse(output.getvalue().encode('utf-8'), content_type='text/csv')
            return response
        except Exception as err:
            (err)
            LOGGER.error('#VoucherCodeDownloadError: %s', err)
            err_return = HttpResponse()
            err_return.status_code = 500
            return err_return


class PreapprovedAddressGenerateView(SuperUserRequiredMixin, CreateView, ListView, ContextMixin):
    template_name = "staff/preapproved_addresses.html"
    form_class = PreapprovedAddressGenerateForm
    queryset = PreapprovedAddress.objects.order_by('addr1', 'addr2')

    def get_success_url(self):
        return '{}?{}'.format(
            reverse('staff:address-whitelist'),
            urllib.parse.urlencode({
                'success': True,
                'note': self.object.note,
            })
        )

    def get_context_data(self, **kwargs):
        self.object_list = PreapprovedAddress.objects.order_by('addr1', 'addr2')
        context = super(PreapprovedAddressGenerateView,
                        self).get_context_data(**kwargs)

        # Add vars for success alert if codes were just generated
        if (self.request.GET.get('success')) == str(True):
            context['success'] = True
            context['note'] = self.request.GET.get('note')

        context['usps_id'] = os.environ['USPS_USER_ID']
        return context
