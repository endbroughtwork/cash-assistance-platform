"""
Form generators on the Staff page
"""
import django_tables2 as tables
from django.forms import ModelForm
from django.forms import Select
from django.forms import TextInput
from django.utils import translation
from django.utils.translation import ugettext_lazy as _
from django_tables2 import CheckBoxColumn
from django_tables2 import Column
from django_tables2 import TemplateColumn
from localflavor.us.forms import USStateField
from localflavor.us.us_states import US_STATES

from app_ccf.models import Application
from app_ccf.models import PreapprovedAddress
from app_ccf.models import VoucherCodeBatch


class NameColumn(Column):
    def render(self, record):
        return '{}, {}'.format(record.last_name, record.first_name)


class AddrColumn(Column):
    def render(self, record):
        return record.get_full_address()


class ApprovedApplicationTable(tables.Table):
    #  selection = CheckBoxColumn(
    #      accessor='application_id',
    #      attrs={
    #          "th__input": {
    #              "onclick": "toggle(this)"
    #          }
    #      }, orderable=False
    #  )
    name = NameColumn(order_by="last_name", verbose_name="Name", empty_values=())
    note = TemplateColumn('{{ record.note|linebreaksbr|truncatechars:150 }}')
    submitted_date = TemplateColumn('{{ record.submitted_date|date:"c" }}',
                                    attrs={"td": {"id": "submitted_dt"}}, verbose_name="Submit Date")
    status_last_modified = TemplateColumn('{{ record.status_last_modified|date:"c" }}',
                                          attrs={"td": {"id": "updated_dt"}}, verbose_name="Last Updated")
    voucher = Column(accessor="vouchercode_str", verbose_name="Code")
    affiliate = Column(accessor="vouchercode.affiliate.name", verbose_name="Affiliate", orderable=False)
    affiliate_id = Column(accessor="vouchercode.affiliate.distributor_id", verbose_name="Affiliate ID", orderable=False)
    phone = Column(accessor="phone_number", verbose_name="Phone")
    full_addr = AddrColumn(verbose_name="Address", orderable=False, empty_values=())

    class Meta:
        model = Application
        template_name = 'django_tables2/bootstrap4.html'
        #  sequence = ("selection", "name", "affiliate_id", "affiliate", "submitted_date", "status", "note", "voucher")
        sequence = ("name", "affiliate_id", "affiliate", "submitted_date", "status_last_modified", "status", "note", "voucher")
        exclude = ("first_name", "last_name", "phone_number")


def get_states_in(language):
    """Returns US states in the provided language, forcing eager evaluation."""
    with translation.override(language):
        return list((abbr, str(name)) for abbr, name in US_STATES[:])


class VoucherCodeGenerateForm(ModelForm):
    """
    Form to create voucher codes for use by applicants
    """
    class Meta:
        """
        VoucherCodeGenerateForm meta info (django)
        """
        model = VoucherCodeBatch
        fields = ['num_codes', 'affiliate', 'campaign',
                  'channel', 'base_amount', 'expiration_date']
        widgets = {
            'num_codes': TextInput(attrs={'class': 'form-control',
                                          'type': 'number', 'size': 80}),
            # TODO: change campaign to list of projects administered by BaltProm
            'campaign': TextInput(attrs={'class': 'form-control'}),
            # TODO: change channel to null or dropdown
            'channel': TextInput(attrs={'class': 'form-control'}),
            'base_amount': TextInput(attrs={'class': 'form-control',
                                            'type': 'number', 'size': 80}),
            'expiration_date': TextInput(attrs={'type': 'date',
                                                'placeholder': 'yyyy-mm-dd'}),
        }


class PreapprovedAddressGenerateForm(ModelForm):
    """
    Generate address validator form
    """
    state_choices = get_states_in('en')
    state_choices.insert(0, ('', _('select_one')))
    state = USStateField(widget=Select(
        attrs={'class': 'form-control'}, choices=state_choices))

    class Meta:
        """
        PreapprovedAddressGenerateForm meta info (django)
        """
        model = PreapprovedAddress
        fields = ['addr1', 'addr2', 'city', 'state', 'zip_code', 'note']
        widgets = {
            'addr1': TextInput(attrs={'class': 'form-control'}),
            'addr2': TextInput(attrs={'class': 'form-control'}),
            'city': TextInput(attrs={'class': 'form-control'}),
            'zip_code': TextInput(attrs={'class': 'form-control'}),
            'note': TextInput(attrs={'class': 'form-control'}),
        }
