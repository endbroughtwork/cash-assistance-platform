"""
Permission mixins restricting views
"""
from django.contrib.auth.mixins import LoginRequiredMixin
from two_factor.views.mixins import OTPRequiredMixin
#  from django.shortcuts import render


class CBORequiredMixin(OTPRequiredMixin, LoginRequiredMixin):
    """Mixin that restricts elements to minimum-CBO permissions"""

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff and not request.user.is_active:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class StaffRequiredMixin(OTPRequiredMixin, LoginRequiredMixin):
    """Mixin that restricts elements to minimum-staff permissions"""

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff and not request.user.is_active:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class SuperUserRequiredMixin(StaffRequiredMixin):
    """Mixin that restricts elements to minimum-superuser permissions"""

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser or not request.user.is_active:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)
