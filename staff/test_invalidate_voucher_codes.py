from datetime import datetime
from datetime import timedelta
from datetime import timezone

from .utils import invalidate_voucher_codes_with_campaign
from .utils import invalidate_voucher_codes_with_code_list
from app_ccf import base_test
from app_ccf.models import DistributorXRef
from app_ccf.models import VoucherCode
from app_ccf.models import VoucherCodeBatch


class InvalidateVoucherCodesTests(base_test.CcfBaseTest):

    def test_invalidate_voucher_codes_with_campaign(self):
        DistributorXRef.objects.bulk_create([
            DistributorXRef("234567", "affx1"),
            DistributorXRef("345678", "affx2")
        ])
        COMBOS = [
            (DistributorXRef.objects.get(distributor_id='234567'), 'cama'),
            (DistributorXRef.objects.get(distributor_id='234567'), 'camb'),
            (DistributorXRef.objects.get(distributor_id='345678'), 'cama'),
            (DistributorXRef.objects.get(distributor_id='345678'), 'camb'),
        ]

        TESTCODES = [
            ('codnumone'),
            ('codnumtwo'),
            ('codnumtre'),
            ('codnumfor')
        ]
        fields = {
            'base_amount': 400,
            'num_codes': 1,
            'code_length': 9,
            'created': datetime.now(timezone.utc),
            'expiration_date': datetime.now(timezone.utc) + timedelta(days=1),
            'channel': 'test_channel'
        }
        voucher_codes = []
        count = 0
        for affiliate, campaign in COMBOS:
            voucher_code = VoucherCode.objects.create(
                added_amount=0,
                code=TESTCODES[count],
                #  affiliate[-1] + campaign[-1],
                batch=VoucherCodeBatch.objects.create(
                    **fields,
                    affiliate=affiliate,
                    campaign=campaign,
                )
            )
            count += 1
            voucher_code.full_clean()
            voucher_codes.append(voucher_code)

        target_affiliate, target_campaign = COMBOS[2]
        invalidate_voucher_codes_with_campaign(affiliate=target_affiliate,
                                               campaign=target_campaign)

        for voucher_code in voucher_codes:
            voucher_code.refresh_from_db()
            if (voucher_code.affiliate == target_affiliate
                    and voucher_code.campaign == target_campaign):
                self.assertFalse(voucher_code.is_active)
            else:
                self.assertTrue(voucher_code.is_active)

    def test_invalidate_voucher_codes_with_code_list(self):
        CODE_A = 'abcdefghi'
        CODE_B = 'qrstuvwxy'
        CODE_C = 'efghijkxm'

        fields = {
            'added_amount': 0,
            'batch': VoucherCodeBatch.objects.create(
                num_codes=1,
                code_length=9,
                base_amount=400,
                created=datetime.now(),
                expiration_date=datetime.now() + timedelta(days=1))
        }
        VoucherCode.objects.create(
            **fields,
            code=CODE_A).full_clean()
        VoucherCode.objects.create(
            **fields,
            code=CODE_B).full_clean()
        VoucherCode.objects.create(
            **fields,
            code=CODE_C).full_clean()

        invalidate_voucher_codes_with_code_list([CODE_A, CODE_B])

        self.assertFalse(VoucherCode.objects.get(code=CODE_A).is_active)
        self.assertFalse(VoucherCode.objects.get(code=CODE_B).is_active)
        self.assertTrue(VoucherCode.objects.get(code=CODE_C).is_active)
