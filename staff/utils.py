import json
import logging
import random
from datetime import datetime
from datetime import timedelta
from datetime import timezone

import boto3
from botocore.exceptions import ClientError
from backoff import expo
from backoff import on_exception
from django.core.exceptions import ValidationError
from ratelimit import limits
from ratelimit import RateLimitException

import ccf.settings as conf
from app_ccf.models import VoucherCode
#  import pprint


#  PPRINTER = pprint.PrettyPrinter(indent=2)
LOGGER = logging.getLogger(__name__)
lambda_client = boto3.client('lambda', region_name='us-east-1')
s3_client = boto3.client('s3', region_name='us-east-1')


def get_archived_payment_actions():
    """Retrieve list of payment action CSV exports

    :returns: List of S3 object metadata dicts with name, upload date
    """
    paginator = s3_client.get_paginator('list_objects_v2')
    bucket = 'com-baltimores-promise-lockbox'
    folder = 'payment_archive/{}/'.format(conf.STAGE)
    response = paginator.paginate(Bucket=bucket, Prefix=folder)
    files = []
    for result in response:
        for entry in result['Contents']:
            file = {
                'Name': entry['Key'].split('/')[-1],
                'Date': entry['LastModified'].astimezone(timezone(-timedelta(hours=5), "EST")),
                'Link': s3_client.generate_presigned_url('get_object',
                                                         Params={'Bucket': bucket,
                                                                 'Key': entry['Key']},
                                                         ExpiresIn=3600)
            }
            files.append(file)
    return files


def upload_payment_action_archive(data, name):
    """Upload the payment action list to S3 for archival

    :data: utf-8 encoded string
    """
    bucket = 'com-baltimores-promise-lockbox'
    folder = 'payment_archive'
    key = '{}/{}/({})_{}'.format(folder, conf.STAGE, 0, name)
    copy_number = 0

    # NOTE: need to check if an archive already exists since upload names
    # have daily granularity, increments copy number until it gets to a free
    # number
    while s3_file_exists(bucket, key):
        copy_number += 1
        key = '{}/{}/({})_{}'.format(folder, conf.STAGE, copy_number, name)

    s3_client.put_object(Key=key,
                         ACL='private',
                         Bucket=bucket,
                         Body=data,
                         ContentType='text/csv',
                         ServerSideEncryption='AES256')


def s3_file_exists(bucket, key):
    """return the key's size if it exist, else None"""
    try:
        obj = s3_client.head_object(Bucket=bucket, Key=key)
        return True
    except ClientError as exc:
        if exc.response['Error']['Code'] != '404':
            return False


def import_voucher_codes(filename, batch):
    LOGGER.info('Uploading codes...')
    num_valid_codes = 0
    num_invalid_codes = 0
    with open(filename, 'r') as f:
        voucher_codes_to_write = []
        for line in f:
            voucher_code_str = line.strip()
            voucher_code = VoucherCode(
                code=voucher_code_str,
                added_amount=0,
                batch=batch,
                is_active=True,
            )
            try:
                voucher_code.full_clean()  # Validate code format
            except ValidationError as e:
                LOGGER.error('Error importing code \'%s\': %s' % (
                    voucher_code_str, str(e)))
                num_invalid_codes += 1
            else:
                voucher_codes_to_write.append(voucher_code)
                LOGGER.info('Imported code \'%s\'.' % voucher_code_str)
                num_valid_codes += 1

    LOGGER.info('Writing %d codes to database (%d invalid)...' %
                (num_valid_codes, num_invalid_codes))
    VoucherCode.objects.bulk_create(voucher_codes_to_write)

    LOGGER.info('Done.')
    LOGGER.debug('Current codes: %s' % VoucherCode.objects.filter())


def generate_voucher_codes(num_codes, code_length, alphabet):
    """Returns a list of new unique codes not already in the database.

    Args:
        num_codes: The number of codes to generate.
        code_length: The number of characters in each code.
        alphabet: A string to choose characters from for each code.
    """
    # Highly unlikely to happen, but we pull up the existing code set to check
    # against in case we generate a duplicate
    code_set = set(VoucherCode.objects.filter().values_list('code', flat=True))
    new_codes = []
    while len(new_codes) < num_codes:
        new_code = ''.join([random.choice(alphabet)
                            for j in range(code_length)])
        if new_code not in code_set:
            new_codes.append(new_code)
            code_set.add(new_code)
    return new_codes


def invalidate_voucher_codes_with_campaign(affiliate, campaign):
    """Invalidates all codes under the given campaign name."""
    voucher_codes = VoucherCode.objects.filter(batch__affiliate=affiliate,
                                               batch__campaign=campaign)
    LOGGER.info('Found %d codes with affiliate \'%s\', '
                'and campaign \'%s\'. Invalidating...' %
                (len(voucher_codes), affiliate, campaign))
    voucher_codes.update(is_active=False)
    LOGGER.info('Done.')


def invalidate_voucher_codes_with_code_list(codes):
    """Invalidates the codes given in a list. Unknown codes are ignored."""
    voucher_codes = VoucherCode.objects.filter(code__in=codes)
    LOGGER.info('Found %d out of %d provided codes. Invalidating...' %
                (len(voucher_codes), len(codes)))
    voucher_codes.update(is_active=False)
    LOGGER.info('Done.')


create_consumer_str = """
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/xmlschema-instance"
                 xmlns:xsd="http://www.w3.org/2001/xmlschema"
                 xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <createConsumer xmlns="www.paymentdata.com">
      <username>uname</username>
      <password>pwd</password>
      <fname>{}</fname>
      <minit>{}</minit>
      <lname>{}</lname>
      <address>{}</address>
      <address2>{}</address2>
      <city>{}</city>
      <state>{}</state>
      <zip>{}</zip>
      <source>1</source>
      <email>{}</email>
      <SSN></SSN>
      <DOB></DOB>
      <phone>{}</phone>
      <csrID>3bulk</csrID>
      <OFACChk>N</OFACChk>
      <DOBChk>N</DOBChk>
      <SSNChk>N</SSNChk>
      <disId>{disId}</disId>
    </createConsumer>
  </soap12:Body>
</soap12:Envelope>
"""

create_card_str = """
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/xmlschema-instance"
                 xmlns:xsd="http://www.w3.org/2001/xmlschema"
                 xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <createCard xmlns="www.paymentdata.com">
      <username>uname</username>
      <password>pwd</password>
      <resellerId></resellerId>
      <consumerId>{responseId}</consumerId>
      <buyerId></buyerId>
      <shipperId></shipperId>
      <distributorId>{disId}</distributorId>
      <designId>{desId}</designId>
      <embossId></embossId>
      <batch>N</batch>
      <Virtual>N</Virtual>
      <activate>N</activate>
      <printName>Y</printName>
      <expDate></expDate>
      <deliveryCode>1</deliveryCode>
      <loadAmount>{loadAmount}</loadAmount>
    </createCard>
  </soap12:Body>
</soap12:Envelope>
"""

deregister_card_str = """
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/xmlschema-instance"
                 xmlns:xsd="http://www.w3.org/2001/xmlschema"
                 xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <markCardAsUnregistered xmlns="www.paymentdata.com">
      <username>uname</username>
      <password>pwd</password>
      <cardID>{}</cardID>
      <reserved1></reserved1>
      <reserved2></reserved2>
      <reserved3></reserved3>
      <reserved4></reserved4>
    </markCardAsUnregistered>
  </soap12:Body>
</soap12:Envelope>
"""

load_funds_from_distributor_str = """
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/xmlschema-instance"
                 xmlns:xsd="http://www.w3.org/2001/xmlschema"
                 xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <loadFundsFromDistributor xmlns="www.paymentdata.com">
      <username>uname</username>
      <password>pwd</password>
      <toCardID>{cardId}</toCardID>
      <toCard></toCard>
      <amount>{cents}</amount>
      <description>Loaded on {datetime}</description>
      <terminalLocation></terminalLocation>
      <terminalCity></terminalCity>
      <terminalState></terminalState>
      <orderNum></orderNum>
      <distributorId>{disId}</distributorId>
    </loadFundsFromDistributor>
  </soap12:Body>
</soap12:Envelope>
"""


@on_exception(expo, RateLimitException)
@limits(calls=2, period=1)
def send_applicant_info(app, dis_id):
    """Send an applicant's info to card services

    CreateConsumer()
    Description: This method will create a consumer record to use as a card or account holder. A consumer ID # will
    returned to use when using the createCard method (all cards must be tied to a consumer). Response codes 1050 (OFAC
    hit), 1075 (SSN chk fail), 1076 (DOB chk fail), 1077 (failure) will return a consumer ID. The consumer will not be
    active (usable) for 1050, 1075, or 1077 responses. However, the consumer will be active (and usable) for 1076 –
    this will be up to the end user.
    Address checking is performed automatically. Response codes 940, 941, 942 indicate an address problem and should be
    handled accordingly. PO Boxes are not accepted (response code 942).
    URL: https://gateway.cardbillpay.com/cardservices/transferSvc.asmx?op=createConsumer
    Method: POST/SOAP

    :application: QuerySet or Application
    :returns: TODO

    """
    req_body = create_consumer_str.format(app.first_name, "", app.last_name, app.addr1,
                                          app.addr2, app.city, app.state, app.zip_code,
                                          app.email, "%s-%s-%s" % (
                                              app.phone_number[:3], app.phone_number[3:6],
                                              app.phone_number[6:]), disId=dis_id)
    json_str = json.dumps({'body': req_body})
    resp = lambda_client.invoke(
        FunctionName='usio-{}-create-consumer'.format(conf.STAGE),
        InvocationType='RequestResponse',
        LogType='Tail',
        Payload=json_str,
    )
    result = json.loads(resp["Payload"].read())['result']
    return result['responseId']['_text']


def send_card_info(response_id, dis_id):
    """Send card info to create the applicant's card.

    CreateCard()
    Description: This method will create a card for a card or account holder. A card ID # will returned to certain
    methods, although most require the actual card number as entered by the customer.
    URL: https://gateway.cardbillpay.com/cardservices/transferSvc.asmx?op=createCard
    Method: POST/SOAP

    :responseId: str
    :returns: TODO

    """
    req_body = create_card_str.format(loadAmount=0, responseId=response_id, desId="314", disId=dis_id)
    json_str = json.dumps({'body': req_body})
    resp = lambda_client.invoke(
        FunctionName='usio-{}-create-card'.format(conf.STAGE),
        InvocationType='RequestResponse',
        LogType='Tail',
        Payload=json_str,
    )
    result = json.loads(resp["Payload"].read())['result']
    return result['responseCardId']['_text']


def deregister_card(card_id):
    """Update the card to unregistered state

    :cardId: str
    :returns: TODO

    """
    req_body = deregister_card_str.format(card_id)
    json_str = json.dumps({'body': req_body})
    lambda_client.invoke(
        FunctionName='usio-{}-deregister-card'.format(conf.STAGE),
        InvocationType='RequestResponse',
        LogType='Tail',
        Payload=json_str,
    )


def load_card(card_id, amount, dis_id):
    """Load funds onto the card

    loadFundsFromDistributor()
    Description: This method will Load a card belonging to a different distributor with specified amount, including a
    description.
    URL: https://gateway.cardbillpay.com/cardservices/transferSvc.asmx?op=loadFundsFromDistributor
    Method: POST/SOAP

    :cardId: str
    :returns: None

    """
    req_body = load_funds_from_distributor_str.format(cents=int(amount * 100), cardId=card_id, disId=dis_id,
                                                      datetime=datetime.now(timezone.utc).isoformat())
    json_str = json.dumps({'body': req_body})
    lambda_client.invoke(
        FunctionName='usio-{}-load-card'.format(conf.STAGE),
        InvocationType='RequestResponse',
        LogType='Tail',
        Payload=json_str,
    )
