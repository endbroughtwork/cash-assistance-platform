from datetime import datetime
from datetime import timedelta
from datetime import timezone

from .utils import generate_voucher_codes
from .utils import import_voucher_codes
from app_ccf import base_test
from app_ccf.models import DistributorXRef
from app_ccf.models import VoucherCode
from app_ccf.models import VoucherCodeBatch

LANGUAGES = ({'language': 'en', }, {'language': 'es'},)


class VoucherUtils(base_test.CcfBaseTest):

    def test_import_voucher_codes(self):
        # test file has 10 valid entries
        codes = './staff/codes_for_import_testing.txt'

        fields = {
            'base_amount': 400,
            'num_codes': 10,
            'code_length': 10,
            'created': datetime.now(timezone.utc),
            'expiration_date': datetime.now(timezone.utc) + timedelta(days=1),
            'channel': 'test_channel'
        }

        affiliate = DistributorXRef.objects.create(
            distributor_id="123456",
            name="Test Place"
        )

        batch = VoucherCodeBatch.objects.create(
            **fields,
            affiliate=affiliate,
            campaign="cares")

        num_codes = import_voucher_codes(codes, batch)
        # check how many imported
        self.assertEqual(batch.num_codes, 10)

    def test_generate_voucher_codes(self):
        num_codes = 20
        code_length = 9
        alphabet = 'abcdefghijkmnopqrstuvwxyz'

        codes = generate_voucher_codes(num_codes, code_length, alphabet)

        self.assertEqual(len(codes), num_codes)
        self.assertEqual(len(codes[2]), code_length)
        # run loop to test for 'l' (should find no 'l')
        for code in codes:
            self.assertTrue('l' not in code)

        num_codes = 10
        code_length = 15
        alphabet = 'abcdefghijkmnopqrstuvwxyz'

        codes = generate_voucher_codes(num_codes, code_length, alphabet)

        self.assertEqual(len(codes), num_codes)
        self.assertEqual(len(codes[5]), code_length)
        # run loop to test for 'l' (should find no 'l')
        for code in codes:
            self.assertTrue('l' not in code)
