# Generated by Django 3.1.5 on 2021-02-01 16:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('staff', '0006_auto_20210115_1651'),
    ]

    operations = [
        migrations.AlterField(
            model_name='adminaction',
            name='action_type',
            field=models.CharField(choices=[('manual_edit', 'Manual Edit'), ('automated_flag', 'Automated Flag'), ('lambda', 'Lambda')], max_length=40),
        ),
    ]
